/* eslint-disable padded-blocks */
/* eslint-disable space-before-function-paren */
/* eslint-disable no-trailing-spaces */
'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');

var app = module.exports = loopback();

app.start = () => {
  // start the web server
  return app.listen(function () {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, (err) => {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module) {
    app.start();
    // app.io = require('socket.io')(app.start());
    // require('socketio-auth')(app.io, {
    //   authenticate: function (socket, value, callback) {
    //     var AccessToken = app.models.AccessToken;
    //             // get credentials sent by the client
    //     var token = AccessToken.find({
    //       where: {
    //         and: [{userId: value.userId}, {id: value.id}],
    //       },
    //     }, function (err, tokenDetail) {
    //       if (err) throw err;
    //       if (tokenDetail.length) {
    //         console.log(tokenDetail);
    //         callback(null, true);
    //       } else {
    //         callback(null, false);
    //       }
    //     }); // find function..    
    //   }, // authenticate function..
    // });

    // app.io.on('connect', function (socket) {
    //   console.log('a user connected');
    //   socket.on('disconnect', function () {
    //     console.log('user disconnected');
    //   });

    //   socket.on('testButton', (data) => {
    //     console.log('clicked with data recieved: ' + data);
    //   });

    //   socket.on('newPassenger', data => {
    //     console.log('data: ', data);
    //     socket.broadcast.emit('newPassenger', data);
    //   });

    //   socket.on('requestAccepted', data => {
    //     console.log('data: ', data);
    //     socket.broadcast.emit('requestAccepted', data);
    //   });
    // });
  }
});
