/* eslint-disable max-len */
/* eslint-disable indent */
// https://www.movable-type.co.uk/scripts/latlong.html

// const pos1 = { latitude: 50.880605, longitude: 4.700618 }
// const pos2 = { latitude: 50.849855, longitude: 4.351988 }

'use strict';
module.exports = {

    checkIfInRange: (firstPos, secondPos, maxDistanceMeters) => {
        const degreesToRadians = degrees => {
            const pi = Math.PI;
            return degrees * (pi / 180);
        };
        let result = false;

        let lat1 = degreesToRadians(firstPos.lat);
        let lat2 = degreesToRadians(secondPos.lat);

        let deltaLon = degreesToRadians(secondPos.lng - firstPos.lng);
        let R = 6371; // gives d in metres
        var d = Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(deltaLon)) * R;

        if (d <= maxDistanceMeters) {
            result = true;
        }

        return result;
    },

};

// const result = calculateDistance(pos1, pos2, 24);
// console.log(result);