/* eslint-disable padded-blocks */
/* eslint-disable object-curly-spacing */
/* eslint-disable space-before-function-paren */
/* eslint-disable indent */
/* eslint-disable no-multiple-empty-lines */
/* eslint-disable no-trailing-spaces */
'use strict';

module.exports = function (Person) {

    Person.afterRemote('login', (ctx, result, next) => {
        const AccessToken = Person.app.models.AccessToken;
        console.log(ctx);
        AccessToken.destroyAll(
            {   
                'userId': ctx.result.userId,
                'id': {
                    neq: ctx.result.id,
                },
                
            }, function (err, res) {
                next();
            });


    });

    Person.getPassengersFromDriverRides = function (userId, cb) {
        const Ride = Person.app.models.Ride;
        const Passenger = Person.app.models.Passenger;
        let driverRides;
        let passengers = [];
        let p = [];
        Ride.find({
            where: {
                driverId: userId,
            }, include: 'passengers',
        }, function (err, response) {
            response.forEach((ride) => {
                let r = ride.toJSON();
                passengers = passengers.concat(r.passengers);
            });

            cb(null, passengers);
        });
    };

    Person.remoteMethod(
        'getPassengersFromDriverRides',
        {
            http: { path: '/getPassengersFromDriverRides', verb: 'get' },
            accepts: { arg: 'id', type: 'string', http: { source: 'query' } },
            returns: { arg: 'passengers', type: 'array' },
        }
    );
};
