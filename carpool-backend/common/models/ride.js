/* eslint-disable max-len */
/* eslint-disable object-curly-spacing */
/* eslint-disable space-before-function-paren */
'use strict';
const { Expo } = require('expo-server-sdk');
const RideFinder = require('../../server/RideFinder');
const moment = require('moment');
module.exports = function (Ride) {
  Ride.observe('before delete', function (ctx, next) {
    const Passenger = Ride.app.models.Passenger;
    const Person = Ride.app.models.Person;
    console.log("delete");
    const Notification = Ride.app.models.Notification;
    console.log(ctx);
    Passenger.find({
      where: {
        rideId: ctx.where.id,
      },
      include: 'person',
    }, function (err, res) {
      console.log(res);
      let messages = [];

      res.forEach(pas => {
        const passenger = pas.toJSON();
        let message = {
          "to": passenger.person.notificationToken,
          "sound": "default",
          "title": "Ride Canceled",
          "body": `Your ride to ${passenger.destination["structured_formatting"]["main_text"]} ${(moment(pas.startTime).isBefore(moment()))?"has been canceled": "was removed"}`,
          "data": { "canceled": true },
          "channelId": "passenger-request",
          "priority": "high"
        }

        messages.push(message);
        console.log(message)
        Notification.create({
          title: message.title,
          body: message.body,
          data: message.data,
          personId: passenger.person.id,
          sendDate: moment()

        }, function (err, res) {

        });

        Ride.sendPushNotification(messages);
        Passenger.destroyAll({
          rideId: ctx.where.id,

        }, function (err, ress) {
          console.log(ress);
          console.log("next");
          console.log(err);
        })




      });
    });
    next();
  })

  Ride.findRides = function (start, destination, startTime, userId, cb) {
    let rides = [];

    Ride.find({
      where: {
        startTime: {
          gte: startTime,
        },
        driverId:{
          neq: userId
        }
      },
    }, function (err, res) {
      res.forEach(ride => {
        if (RideFinder.checkIfInRange(ride.start.geometry.location, start.geometry.location, 2) && RideFinder.checkIfInRange(ride.destination.geometry.location, destination.geometry.location, 2)) {
          console.log('success');
          rides.push(ride);
        }
      });
      cb(null, rides);
    });
  };

  Ride.sendPushNotification = function (messages) {
    let expo = new Expo();

    let chunks = expo.chunkPushNotifications(messages);

    let tickets = [];

    (async () => {
      for (let chunk of chunks) {
        try {
          let ticketChunk = await expo.sendPushNotificationsAsync(chunk);
          console.log(ticketChunk);
          tickets.push(...ticketChunk);
        } catch (error) {
          console.log("error " + error);
        }
      }
    })();

    let receiptIds = [];
    for (let ticket of tickets) {
      // NOTE: Not all tickets have IDs; for example, tickets for notifications
      // that could not be enqueued will have error information and no receipt ID.
      if (ticket.id) {
        receiptIds.push(ticket.id);
      }
    }

    let receiptIdChunks = expo.chunkPushNotificationReceiptIds(receiptIds);
    (async () => {
      // Like sending notifications, there are different strategies you could use
      // to retrieve batches of receipts from the Expo service.
      for (let chunk of receiptIdChunks) {
        try {
          let receipts = await expo.getPushNotificationReceiptsAsync(chunk);
          console.log(receipts);

          // The receipts specify whether Apple or Google successfully received the
          // notification and information about an error, if one occurred.
          for (let receipt of receipts) {
            if (receipt.status === 'ok') {
              continue;
            } else if (receipt.status === 'error') {
              console.error(`There was an error sending a notification: ${receipt.message}`);
              if (receipt.details && receipt.details.error) {
                // The error codes are listed in the Expo documentation:
                // https://docs.expo.io/versions/latest/guides/push-notifications#response-format
                // You must handle the errors appropriately.
                console.error(`The error code is ${receipt.details.error}`);
              }
            }
          }
        } catch (error) {
          console.error(error);
        }
      }
    })();

  }

  Ride.remoteMethod('findRides', {
    accepts: [
      { arg: 'start', type: 'object' },
      { arg: 'destination', type: 'object' },
      { arg: 'startTime', type: 'date' },
      { arg: 'userId', type: 'string' },
    ],
    returns: { arg: 'rides', type: 'array' },
  });
};
