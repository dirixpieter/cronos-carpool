/* eslint-disable no-undef */
/* eslint-disable indent */
'use strict';
const { Expo } = require('expo-server-sdk');
const moment = require('moment');
module.exports = function (Passenger) {

    /**
 * create a new passenger and send a notification to the driver
 * @param {object} passenger the passenger you are inserting
 * @param {string} driverId driverId
 * @param {Function(Error, object)} callback
 */

    Passenger.createWithNotif = function (passenger, driverId, callback) {
        const Person = Passenger.app.models.Person;
        const Ride = Passenger.app.models.Ride;
        const Notification = Passenger.app.models.Notification;
        Ride.findById(passenger.rideId, function (err, res) {
            let obj = res;

            if ((obj.freeSeats -= 1) >= 0) {

                Ride.upsert(obj, function (errr, ress) {
                    // console.log(ress.freeSeats);
                    Passenger.create(passenger, function (error, response) {

                        Person.findById(driverId, function (err, res) {
                           
                            let message = {
                                "to": res.notificationToken,
                                "sound": "default",
                                "title": "New ride along request",
                                "body": `Someone wants to ride along to ${obj.destination["structured_formatting"]["main_text"]}`,
                                "data": { "rideId": passenger.rideId },
                                "channelId": "passenger-request",
                                "priority": "high"
                            }
                            Notification.create({
                                title: message.title,
                                body: message.body,
                                data: message.data,
                                personId: driverId,
                                sendDate: moment()
                            }, function(err, res) {
                                
                            })

                            Passenger.sendPushNotification(message);
                        });
                        callback(null, response);
                    });
                })
            } else {
                callback({ error: "seat limit reached" }, null);
            }

            if (res.freeSeats > 0) {


            }

        })


    };


    /**
    * denies the selected passenger
    * @param {object} passenger the passenger you like to delete, including it's user so that he can be notified
    * @param {Function(Error)} callback
    */

    Passenger.denyWithNotif = function (passenger, callback) {
        const Ride = Passenger.app.models.Ride;
        const Notification = Passenger.app.models.Notification;
        Ride.findById(passenger.rideId, function (err, res) {
            let obj = res;

            obj.freeSeats++;
            Ride.upsert(obj, function (errr, ress) {
                let message = {
                    "to": passenger.person.notificationToken,
                    "sound": "default",
                    "title": "Your request has been denied",
                    "body": `Your request for riding along to ${passenger.destination["structured_formatting"]["main_text"]}`,
                    "data": { "denied": true },
                    "channelId": "passenger-request",
                    "priority": "high"
                }
                Notification.create({
                    title: message.title,
                    body: message.body,
                    data: message.data,
                    personId: passenger.person.id,
                    sendDate: moment()
                    
                }, function(err, res) {
                    
                })

                Passenger.sendPushNotification(message);

                Passenger.destroyById(passenger.id, function (err) {
                    console.log("deleted");
                    callback(err);
                })
            })

        })


        // TODO

    };

    /**
    * accepts a passenger
    * @param {object} passenger passenger object with you would like to accept
    * 
    * @param {Function(Error, object)} callback
    */

    Passenger.acceptWithNotif = function (passenger, callback) {
        const Notification = Passenger.app.models.Notification;
        const Person = Passenger.app.models.Person;
        const Ride = Passenger.app.models.Ride;
        Passenger.upsert({ ...passenger, "accepted": true }, function (err, response) {


            Person.findById(passenger.personId, function (err, res) {
                let message = {
                    "to": res.notificationToken,
                    "sound": "default",
                    "title": "Request has been accepted",
                    "body": `Your request for riding along to ${passenger.destination["structured_formatting"]["main_text"]} has been accepted`,
                    "data": { "rideId": passenger.rideId },
                    "channelId": "passenger-request",
                    "priority": "high"
                }
                Notification.create({
                    title: message.title,
                    body: message.body,
                    data: message.data,
                    personId: passenger.personId,
                    sendDate: moment()
                }, function(err, res) {
                    
                })

                Passenger.sendPushNotification(message);
            });

            callback(null, response);
        })
        // TODO

    };

    Passenger.sendPushNotification = function (message) {
        let expo = new Expo();
        let messages = [message];

        let chunks = expo.chunkPushNotifications(messages);

        let tickets = [];

        (async () => {
            for (let chunk of chunks) {
                try {
                    let ticketChunk = await expo.sendPushNotificationsAsync(chunk);
                    console.log(ticketChunk);
                    tickets.push(...ticketChunk);
                } catch (error) {
                    console.log("error " + error);
                }
            }
        })();

        let receiptIds = [];
        for (let ticket of tickets) {
            // NOTE: Not all tickets have IDs; for example, tickets for notifications
            // that could not be enqueued will have error information and no receipt ID.
            if (ticket.id) {
                receiptIds.push(ticket.id);
            }
        }

        let receiptIdChunks = expo.chunkPushNotificationReceiptIds(receiptIds);
        (async () => {
            // Like sending notifications, there are different strategies you could use
            // to retrieve batches of receipts from the Expo service.
            for (let chunk of receiptIdChunks) {
                try {
                    let receipts = await expo.getPushNotificationReceiptsAsync(chunk);
                    console.log(receipts);

                    // The receipts specify whether Apple or Google successfully received the
                    // notification and information about an error, if one occurred.
                    for (let receipt of receipts) {
                        if (receipt.status === 'ok') {
                            continue;
                        } else if (receipt.status === 'error') {
                            console.error(`There was an error sending a notification: ${receipt.message}`);
                            if (receipt.details && receipt.details.error) {
                                // The error codes are listed in the Expo documentation:
                                // https://docs.expo.io/versions/latest/guides/push-notifications#response-format
                                // You must handle the errors appropriately.
                                console.error(`The error code is ${receipt.details.error}`);
                            }
                        }
                    }
                } catch (error) {
                    console.error(error);
                }
            }
        })();

    }
};
