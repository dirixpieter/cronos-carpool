import * as types from '../actions/action-types';

const initialState = {
    location: {},
    loading: true,//true,
    markers: []

};


const LocationReducer = (state = initialState, action) => {
    switch (action.type) {

        case types.SET_CURRENT_LOCATION:

            return {
                ...state,
                location: action.payload,
                loading: false
            }
        
        case types.SET_MARKERS:
            let m = state.markers;
            let a = action.payload;
            if(m.length > 2) {
                a.shift()
            }
            return {
                ...state,
                markers: a
            }
        default:
            return state;
    }
}

export default LocationReducer;