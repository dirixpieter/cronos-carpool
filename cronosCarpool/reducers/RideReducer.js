import * as types from '../actions/action-types';
import {AsyncStorage} from 'react-native';
import moment from 'moment'
const initialState = {
    newRide: {
        start: {
            "description": "start",
        },
        destination: {
            "description": "destination",
        },
        startTime: moment(),
        freeSeats: 1,
        driverId: "",
        departAt: true
    },
    newRoundTrip: {
        start: {
            "description": "start",
        },
        destination: {
            "description": "destination",
        },
        startTime: moment(),
        freeSeats: 1,
        driverId: "",
        departAt: true
    },
    driverRides: [],
    loadingDriverRides: true,
    query: {
        "start":{
            description: "start",
        },
        destination:{
            description: "destination",
        },
        startTime: moment()

    },
    results: [],
    passengers: {
        loading: true,
        data: []
    },
    passengerRides: [],
    nrOfPassengers: 0,
    loadingPassengerRides: true
    


};


const RideReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.NEW_ROUND_TRIP:
            return {
                ...state,
                newRoundTrip: action.payload
            }
        case types.ON_RIDE_INPUT_CHANGE:

            return {
                ...state,
                newRide: action.payload
            }

        case types.FIND_RIDE_QUERY:

            return {
                ...state,
                query: action.payload
            }
        case types.SET_RIDES_AS_DRIVER:

            return {
                ...state,
                driverRides: action.payload,
                loadingDriverRides: false
            }

        case types.SET_RIDES_AS_PASSENGER:

            return {
                ...state,
                passengerRides: action.payload,
                loadingPassengerRides: false
            }

        case types.NR_OF_PASSENGERS:

            AsyncStorage.setItem('nr_of_passengers', JSON.stringify(action.payload));

            return {
                ...state,
                nrOfPassengers: action.payload
            }


        case types.SET_RESULTS:

            return {
                ...state,
                results: action.payload
            }

        case types.SET_PASSENGERS:

            return {
                ...state,
                passengers: {
                    data: action.payload,
                    loading: false
                }
            }
        default:
            return state;
    }
}

export default RideReducer;