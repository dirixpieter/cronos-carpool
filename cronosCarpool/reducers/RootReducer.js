import { combineReducers } from 'redux';
import test from './TestReducer';
import ride from './RideReducer';
import user from './UserReducer';
import location from './LocationReducer';
import notifications from './NotificationReducer';

export default combineReducers({
    test: test,
    ride: ride,
    user: user,
    location: location,
    notifications: notifications
})