import * as types from '../actions/action-types';

const initialState = {

};

const TestReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.TEST:
            let newTest = state.test + 1;
            return {
                ...state,
                test: newTest
            }

        default: 
            return state;
    }
}

export default TestReducer;