import * as types from '../actions/action-types';
import { AsyncStorage } from 'react-native';

const initialState = {
    
    user: {},
    access_token: {},
    loginInput: {},
    registerInput: {},
    loggedIn: false,
    cars: [],
    car: {
        model: "",
        licensePlate: "",
        colour: "",
        freeSeats: ""
    }


};

const UserReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.ADD_NOTIFTOKEN:
            return {
                ...state,
                user: {
                    ...state.user,
                    notificationToken: action.payload
                }
            }

        case types.ON_LOGIN_INPUT_CHANGE:
            return {
                ...state,
                loginInput: action.payload
            }

        case types.ON_REGISTER_INPUT_CHANGE:
            return {
                ...state,
                registerInput: action.payload
            }

        case types.LOGIN_SUCCES:
            let a = {
                "created": action.payload.created,
                "id": action.payload.id,
                "ttl": action.payload.ttl,
                "userId": action.payload.userId
            }
            let user = action.payload.user;
            user = {
                ...user,
                access_token: a
            }
            AsyncStorage.setItem('access_token', JSON.stringify(a));
            AsyncStorage.setItem('user', JSON.stringify(user));
            return {
                ...state,
                user: user,
                access_token: a,
                loggedIn: true
            }

        case types.CURRENT_USER:
            AsyncStorage.setItem('user', JSON.stringify(action.payload));
            return {
                ...state,
                user: action.payload
            }
        case types.SET_CARS:
            return {
                ...state,
                cars: action.payload
            }

        case types.ON_CAR_INPUT_CHANGE:
            return {
                ...state,
                car: action.payload
            }
        default:
            return state;
    }
}

export default UserReducer;