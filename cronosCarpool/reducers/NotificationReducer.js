import *as types from '../actions/action-types'

const initialState = {
    notifications: [],
    loadingNotifs: true,
    newNotif: false
}

export default (state = initialState, action) => {
    switch (action.type) {

        case types.SET_NOTIFICATIONS:
            return {
                ...state,
                notifications: action.payload,
                loadingNotifs: false
            };

            case types.NEW_NOTIF:
            return {
                ...state,
                newNotif: action.payload,
            };

        default:
            return state
    }
};
