export function getRegionForCoordinates(points) { //https://github.com/react-native-community/react-native-maps/issues/505
    // points should be an array of { lat: X, lng: Y }
    let minX, maxX, minY, maxY;
   
    // init first point
    ((point) => {
        minX = point.lat;
        maxX = point.lat;
        minY = point.lng;
        maxY = point.lng;
    })(points[0]);

    // calculate rect
    points.map((point) => {
        minX = Math.min(minX, point.lat);
        maxX = Math.max(maxX, point.lat);
        minY = Math.min(minY, point.lng);
        maxY = Math.max(maxY, point.lng);
    });

    const midX = (minX + maxX) / 2;
    const midY = (minY + maxY) / 2;
    const deltaX = (maxX - minX) + 0.1;
    const deltaY = (maxY - minY) + 0.1;

    return {
        latitude: midX,
        longitude: midY,
        latitudeDelta: deltaX,
        longitudeDelta: deltaY
    };
}