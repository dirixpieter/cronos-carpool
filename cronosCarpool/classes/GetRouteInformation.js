import axios from 'axios';
import polyline from '@mapbox/polyline';

export async function getRideInfo(ride) {
  
    let resObject = {
        decodedPolyline: [],
        duration: {},
        distance: {}

    }
    const origin = `place_id:${ride.start["place_id"]}`
    const destination = `place_id:${ride.destination["place_id"]}`
    const url = `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&mode=driving&key=AIzaSyAibRKIa1WNaDhORYI0DltaKtQmRDtFOS8`
    
    await axios.get(url)
        .then((response) => {

            let points = polyline.decode(response.data.routes[0]["overview_polyline"].points);
            resObject.decodedPolyline = points.map((point, index) => {
                return {
                    latitude: point[0],
                    longitude: point[1]
                }
            })
            resObject.duration = response.data.routes[0].legs[0].duration;
            resObject.distance = response.data.routes[0].legs[0].distance
        })
        .catch((error) => {
        })

    return resObject

}