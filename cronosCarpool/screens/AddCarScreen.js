import React, { Component } from 'react';
import { KeyboardAvoidingView, View, StyleSheet } from 'react-native';
import { Button, Input, ListItem, Text } from 'react-native-elements';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { onCarInputChange, setCars } from '../actions/user-actions';
import { createCar } from '../services/CarService';
import BottomButtons from '../stylesheets/BottomButtons';
import LoginRegisterFields from '../stylesheets/LoginRegisterFields';
class AddCarScreen extends Component {

    constructor(props) {
        super(props);


    }


    static navigationOptions = {
        title: "Add Car",
        headerStyle: {
            backgroundColor: '#212121',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    componentDidMount = () => {
    };

    renderItem = ({ item }) => (
        <ListItem
            title={item.model}
            subtitle={item.colour}
            rightTitle={item.licensePlate}
            rightSubtitle={item.freeSeats.toString}
        />
    )

    handleNextPress = async () => {
        const car = this.props.car;
        await createCar(this.props.user.id, car)
            .then(async response => {
                let cars = this.props.cars;
                if (cars.length == 0 || cars == undefined) {
                    cars = [car];
                } else {
                    cars.push(response.data);
                }

                await this.props.setCars(cars);

            })
        const { params } = this.props.navigation.state
        if (params.nextRoute == 'AddRideForm') {
            this.props.navigation.navigate('AddRideForm');
        } else if (params.nextRoute == 'Back') {
            this.props.navigation.dispatch(NavigationActions.back())
        }
    }
    render() {
        const { params } = this.props.navigation.state;
        let car = this.props.car
        return (
            <View style={{ flex: 1, alignItems: 'stretch', padding: 24 }}>
                <KeyboardAvoidingView behavior="position" enabled>
                    <Text style={{ fontWeight: 'bold', fontSize: 18, marginBottom: 24 }}>Add Your Car</Text>
                    <Input
                        containerStyle={[LoginRegisterFields.input]}
                        inputContainerStyle={
                            styles.input
                        }
                        placeholder="Model"
                        onChangeText={(text) => {
                            car = {
                                ...car,
                                model: text
                            }
                            this.props.onCarInputChange(car);
                        }}
                        value={this.props.car.model}
                    />

                    <Input
                        containerStyle={[LoginRegisterFields.input]}
                        inputContainerStyle={
                            styles.input
                        }
                        placeholder="Colour"
                        onChangeText={(text) => {
                            car = {
                                ...car,
                                colour: text
                            }
                            this.props.onCarInputChange(car);
                        }}
                        value={this.props.car.colour}
                    />

                    <Input
                        containerStyle={[LoginRegisterFields.input]}
                        inputContainerStyle={
                            styles.input
                        }
                        placeholder="License Plate"
                        onChangeText={(text) => {
                            car = {
                                ...car,
                                licensePlate: text
                            }
                            this.props.onCarInputChange(car);
                        }}
                        value={this.props.car.licensePlate}

                    />

                    <Input
                        containerStyle={[LoginRegisterFields.input]}
                        inputContainerStyle={
                            styles.input
                        }
                        placeholder="Free Seats"
                        maxLength={2}
                        onChangeText={(text) => {
                            let val;
                            if (text == "") {
                                val = text;
                            } else {
                                val = parseInt(text);
                            }



                            car = {
                                ...car,
                                freeSeats: val
                            }
                            this.props.onCarInputChange(car);
                        }}
                        value={this.props.car.freeSeats.toString()}
                        keyboardType="numeric"
                    />
                    <Button
                        title={(params.nextRoute == 'Back')?"Save":"Next"}
                        buttonStyle={[BottomButtons.topButton, { marginBottom: 24 }]}
                        onPress={() => {
                            const c = this.props.car;
                            if (c.model != "" && c.colour != "" && c.licensePlate != "" && c.freeSeats != 0 && c.freeSeats != null) {
                                this.handleNextPress();
                            }
                        }}

                    />
                </KeyboardAvoidingView>
            </View>


        );
    }
}

const styles = StyleSheet.create({
    input: { borderBottomWidth: 0, paddingLeft: 15 }
})
function mapStateToProps(state) {
    return {
        access_token: state.user,
        user: state.user.user,
        cars: state.user.cars,
        car: state.user.car,
    };
}

const mapDispatchToProps = {
    onCarInputChange,
    setCars
};



export default connect(mapStateToProps, mapDispatchToProps)(AddCarScreen);