import React, { Component } from 'react';
import { Alert, View } from 'react-native';
import { Button, ListItem } from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { currentUser, setCars } from '../../actions/user-actions';
import { getCars, removeCar } from '../../services/CarService';
import BottomButtons from '../../stylesheets/BottomButtons';

class CarScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            refreshing: false,
        }
    }

    onRefresh() {
        this.setState({ refreshing: true }, function () { this.getCars() });
    }

    getCars = () => {
        getCars(this.props.user.id)
            .then((response) => {
                console.log(response.data);
                this.props.setCars(response.data);
            })
    }

    static navigationOptions = {
        title: "Your Cars",
        headerStyle: {
            backgroundColor: '#212121',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    componentDidMount = () => {
        this.onRefresh();
    };

    renderItem = ({ item }) => (
        <ListItem
            key={item.id}
            title={item.model}
            subtitle={item.colour}
            rightTitle={item.licensePlate}
            rightSubtitle={item.freeSeats.toString()}
            onLongPress={() => {
                this.openAlert(item);
            }}
        />
    )

    removeCar = (car) => {
        removeCar(this.props.user.id, car.id)
            .then(() => this.onRefresh())
    }

    openAlert = (item) => {
        Alert.alert(
            'Delete Car',
            `Remove ${item.model} from your profile, it may still be set as the vehicle of one of your rides`,
            [
                {
                    text: 'Cancel',
                },
                {
                    text: 'Ok', onPress: () => this.removeCar(item)
                }
            ]
        )
    }
    render() {

        return (
            <View style={{ flex: 1, alignItems: 'stretch', padding: 24 }}>
                <FlatList
                    contentContainerStyle={{}}
                    data={this.props.cars}
                    extraData={this.props.cars}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => item.id}
                    onRefresh={() => this.onRefresh()}
                    refreshing={this.state.refreshing}
                />
                <Button
                    title="Add Car"
                    buttonStyle={[BottomButtons.topButton, { marginBottom: 0 }]}
                    onPress={() => {
                        this.props.navigation.navigate('AddCar', { nextRoute: 'Back' })
                    }}

                />
            </View>


        );
    }
}


function mapStateToProps(state) {
    return {
        access_token: state.user,
        user: state.user.user,
        cars: state.user.cars,
    };
}

const mapDispatchToProps = {
    currentUser,
    setCars,
};



export default connect(mapStateToProps, mapDispatchToProps)(CarScreen);