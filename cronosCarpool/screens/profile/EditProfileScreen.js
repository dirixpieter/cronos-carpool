import React, { Component } from 'react';
import { View } from 'react-native';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';
import { currentUser } from '../../actions/user-actions';
import EditProfileComp from '../../components/EditProfileComp';
import { update } from '../../services/UserService';
import BottomButtons from '../../stylesheets/BottomButtons';

class EditProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            editable: false,
            saveable: false,
            personInput: this.props.user
        };
    }


    static navigationOptions = ({ navigation }) => {
        return {
            title: "Your Profile",
            headerStyle: {
                backgroundColor: '#212121',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        };
    };

    componentDidMount = () => {
        this.props.navigation.setParams({ enableEdit: this._enableEdit });

    };

    _enableEdit = () => {
        this.setState({
            editable: !this.state.editable
        })

    }


    handleTextChange = person => {
        const p = person;
        const u = this.props.user;
        console.log(p);
        console.log(u);
        this.setState({
            personInput: person
        })

        if (p.name != u.name || p.surname != u.surname || p.email != u.email || p.company != u.company || p.location != u.location || p.cellphone != u.cellphone) {
            this.setState({
                saveable: true
            })
        } else if (p.name === u.name || p.surname === u.surname || p.email === u.email || p.company === u.company || p.location === u.location || p.cellphone === u.cellphone) {
            this.setState({
                saveable: false
            })
        }
    }

    updateUser = () => {
        const {
            access_token,
            ...body
        } = this.state.personInput;

        update(this.props.user.id, body)
            .then((response) => {
                console.log(JSON.stringify(response));
                this.props.currentUser({
                    ...response.data,
                    "access_token": this.props.user["access_token"]
                });

                this.setState({
                    editable: false,
                    saveable: false
                })
            })
            .catch((error) => JSON.stringify(error))

    }

    render() {
        console.log(this.state.saveable);
        return (
            <View style={{ flex: 1, alignItems: 'stretch' }}>

                <EditProfileComp
                    title="Name:"
                    person={this.state.personInput}
                    valKey={"name"}
                    editable={this.state.editable}
                    handleTextChange={this.handleTextChange}
                />

                <EditProfileComp
                    title="Surname:"
                    person={this.state.personInput}
                    valKey={"surname"}
                    editable={this.state.editable}
                    handleTextChange={this.handleTextChange}
                />


                <EditProfileComp
                    title="Email:"
                    person={this.state.personInput}
                    valKey={"email"}
                    editable={this.state.editable}
                    handleTextChange={this.handleTextChange}
                />


                <EditProfileComp
                    title="Company:"
                    person={this.state.personInput}
                    valKey={"company"}
                    editable={this.state.editable}
                    handleTextChange={this.handleTextChange}
                />


                <EditProfileComp
                    title="Location:"
                    person={this.state.personInput}
                    valKey={"location"}
                    editable={this.state.editable}
                    handleTextChange={this.handleTextChange}
                />

                <EditProfileComp
                    title="Phone:"
                    person={this.state.personInput}
                    valKey={"cellphone"}
                    editable={this.state.editable}
                    handleTextChange={this.handleTextChange}
                />
                <View style={{
                    position: "absolute", bottom: 24, left: 24, right: 24
                }}>


                    {(this.state.saveable) ? <Button
                        containerStyle={[{}]}
                        buttonStyle={[BottomButtons.topButton, { marginBottom: 0 }]}
                        title="Save"
                        onPress={
                            () => {
                                this.updateUser();
                            }
                        }

                    /> : null}

                    {(!this.state.editable) ? <Button
                        containerStyle={[{}]}
                        buttonStyle={[BottomButtons.topButton, { marginBottom: 0 }]}
                        title="Edit"
                        onPress={
                            () => {
                                this.setState({
                                    editable: true
                                });
                            }
                        }

                    /> : null}
                </View>
            </View>


        );
    }
}


function mapStateToProps(state) {
    return {
        access_token: state.user,
        user: state.user.user
    };
}

const mapDispatchToProps = {
    currentUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProfileScreen);