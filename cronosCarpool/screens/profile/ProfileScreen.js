import { Ionicons } from '@expo/vector-icons';
import { Constants, IntentLauncherAndroid as IntentLauncher, Linking } from 'expo';
import React, { Component } from 'react';
import { AsyncStorage, Platform, View } from 'react-native';
import { Button, ListItem, Text } from 'react-native-elements';
import { NavigationActions, StackActions } from 'react-navigation';
import { connect } from 'react-redux';
import { currentUser } from '../../actions/user-actions';
import { logout } from '../../services/UserService';
import BottomButtons from '../../stylesheets/BottomButtons';

class ProfileScreen extends Component {

    static navigationOptions = {
        title: "Profile",
        headerStyle: {
            backgroundColor: '#212121',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'LoginRegister' })],
    });



    logOut = async () => {
        let that = this;
        try {
            const value = await AsyncStorage.getItem('access_token');
            if (value !== null) {

                // We have data!!
                logout()
                    .then(response => {

                        that.clearToken();

                    })
                    .catch(error => {
                        console.log(JSON.stringify(error));
                        that.clearToken();

                    })
            }
        } catch (error) {
            console.log("KEY STORAGE: ", JSON.stringify(error))
            // Error retrieving data
            that.clearToken();
        }
        // 

    }


    clearToken = async () => {
        try {
            await AsyncStorage.removeItem('access_token');
            this.props.navigation.dispatch(this.resetAction);
        } catch (error) {
            // Error retrieving data
        }
    }

    componentDidMount() {

        this.user();
    }

    user = async () => {
        try {
            const value = await AsyncStorage.getItem('user');
            if (value !== null) {
                // We have data!!

                this.props.currentUser(JSON.parse(value));
            }
        } catch (error) {
            // Error retrieving data
        }
    }

    openSettings = () => {


        // onClick function to access app's permission settings
        if (Platform.OS == "android") {
            IntentLauncher.startActivityAsync(IntentLauncher.ACTION_APPLICATION_DETAILS_SETTINGS, {},
                'package:' + Constants.manifest.android.package);
        } else {
            Linking.openURL('app-settings:');
        }


    }

    render() {

        return (
            <View style={{ flex: 1, alignItems: 'stretch' }}>
                <View style={{ paddingTop: 32, paddingBottom: 32 }}>
                    <Text h3 style={{ textAlign: "center" }}>{`${this.props.user.name} ${this.props.user.surname}`}</Text>


                </View>
                <ListItem
                    topDivider
                    leftIcon={<Ionicons name="md-person" size={24} color="#212121" />}
                    title="Your Profile"
                    bottomDivider
                    onPress={() => {
                        this.props.navigation.navigate("EditProfile")
                    }}

                />
                <ListItem
                    bottomDivider
                    topDivider
                    title="Your Rides"
                    leftIcon={<Ionicons name="md-list" size={24} color="#212121" />}

                    onPress={() => {
                        this.props.navigation.navigate("My Rides");
                    }}
                />
                <ListItem
                    topDivider
                    title="Your Cars"
                    leftIcon={<Ionicons name="md-car" size={24} color="#212121" />}
                    bottomDivider
                    onPress={() => {
                        this.props.navigation.navigate("Cars");
                    }}
                />
                <ListItem
                    topDivider
                    title="Settings"
                    leftIcon={<Ionicons name="md-settings" size={24} color="#212121" />}
                    bottomDivider
                    onPress={() => {
                        this.openSettings();
                        //this.props.navigation.navigate("Settings");
                    }}
                />
                <Button
                    containerStyle={[{ position: "absolute", bottom: 24, left: 24, right: 24 }]}
                    buttonStyle={[BottomButtons.topButton, { marginBottom: 0 }]}
                    title="Logout"
                    onPress={
                        () => {
                            this.logOut();
                        }
                    }

                />
            </View>


        );
    }
}


function mapStateToProps(state) {
    return {
        access_token: state.user,
        user: state.user.user
    };
}

const mapDispatchToProps = {
    currentUser,
};




export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);