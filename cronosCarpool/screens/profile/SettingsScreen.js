import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { currentUser } from '../../actions/user-actions';

class SettingsScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }


    static navigationOptions = ({ navigation }) => {
        return {
            title: "Settings",
            headerStyle: {
                backgroundColor: '#212121',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            }
        };
    };



    render() {
        return (
            <View style={{ flex: 1, alignItems: 'stretch', padding: 24 }}>

                {/* <Button
                    title="Notification settings"
                    buttonStyle={[BottomButtons.topButton, { marginBottom: 0 }]}
                    onPress={() => {
                        IntentLauncher.startActivityAsync(IntentLauncher.ACTION_APP_NOTIFICATION_SETTINGS, {
                            "android.provider.extra.APP_PACKAGE": "be.craftworkz.cronoscarpool"
                        });
                    }}
                /> */}
            </View>


        );
    }
}


function mapStateToProps(state) {
    return {
        access_token: state.user,
        user: state.user.user
    };
}

const mapDispatchToProps = {
    currentUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(SettingsScreen);