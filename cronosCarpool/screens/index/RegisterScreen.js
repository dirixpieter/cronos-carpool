import React, { Component } from 'react';
import { Alert, View } from 'react-native';
import { Button, Input } from 'react-native-elements';
import { connect } from 'react-redux';
import 'url-search-params-polyfill';
import { onRegisterInputChange } from '../../actions/user-actions';
import { register } from '../../services/UserService';
import BottomButtons from '../../stylesheets/BottomButtons';
import LoginRegisterFields from '../../stylesheets/LoginRegisterFields';
import ToastContext from '../../classes/toast';


class RegisterScreenWithContext extends Component {



    registerNewUser() {
        const i = this.props.registerInput;
        const user = {
            name: i.name.trim(),
            surname: i.surname.trim(),
            email: i.email.trim().toLowerCase(),
            password: i.password.trim()
        }
        register(user)
            .then(response => {
                console.log("response" + JSON.stringify(response));
                const password = this.props.registerInput.password
                this.props.onRegisterInputChange({
                    name: "",
                    surname: "",
                    email: "",
                    password: ""
                });
                this.props.navigation.navigate("Login", { returnedPerson: response.data, password: password })

            })
            .catch(error => {
                console.log("error" + JSON.stringify(error.response))
                if (JSON.stringify(error.request.status) == 422) {
                    Alert.alert(
                        'Error',
                        'A user with this email already exists',
                        [
                            { text: 'OK', onPress: () => console.log('OK Pressed') },
                        ]
                    );
                } else {
                    this.props.values.toast.current.show('Register failed', 1000);
                }

            })

    }

    render() {
        let user = this.props.registerInput

        return (
            <View style={{ flex: 1 }}>
                <View style={{
                    paddingLeft: 25,
                    paddingRight: 25,
                    flex: 1,
                    alignItems: "stretch"
                }}>
                    <View style={{ marginTop: 40, flex: 1 }}>
                        <Input
                            containerStyle={[LoginRegisterFields.input]}
                            inputContainerStyle={
                                { borderBottomWidth: 0, paddingLeft: 15 }
                            }
                            placeholder="Name"
                            onChangeText={(text) => {
                                user = {
                                    ...user,
                                    name: text
                                }
                                this.props.onRegisterInputChange(user);
                            }}
                            value={this.props.registerInput.name}
                        />

                        <Input
                            containerStyle={[LoginRegisterFields.input]}
                            inputContainerStyle={
                                { borderBottomWidth: 0, paddingLeft: 15 }
                            }
                            placeholder="Surname"
                            onChangeText={(text) => {
                                user = {
                                    ...user,
                                    surname: text
                                }
                                this.props.onRegisterInputChange(user);
                            }}
                            value={this.props.registerInput.surname}
                        />

                        <Input
                            containerStyle={[LoginRegisterFields.input]}
                            inputContainerStyle={
                                { borderBottomWidth: 0, paddingLeft: 15 }
                            }
                            placeholder="Email"
                            onChangeText={(text) => {
                                user = {
                                    ...user,
                                    email: text
                                }
                                this.props.onRegisterInputChange(user);
                            }}
                            value={this.props.registerInput.email}
                        />

                        <Input
                            containerStyle={[LoginRegisterFields.input]}
                            inputContainerStyle={
                                { borderBottomWidth: 0, paddingLeft: 15 }
                            }
                            placeholder="Password"
                            onChangeText={(text) => {
                                user = {
                                    ...user,
                                    password: text
                                }
                                this.props.onRegisterInputChange(user);
                            }}
                            secureTextEntry={true}
                            value={this.props.registerInput.password}
                        />

                    </View>

                    <View style={{
                        position: "absolute", bottom: 0, left: 0, right: 0,
                        padding: 24
                    }}>
                        <Button
                            buttonStyle={[BottomButtons.button, BottomButtons.topButton, { borderRadius: 4, marginBottom: 15 }]}
                            title="Register"
                            onPress={() => {
                                if (this.props.registerInput.password != "" && this.props.registerInput.email != "" && this.props.registerInput.name != "" && this.props.registerInput.surname != "") {
                                    this.registerNewUser();
                                }
                                // this.props.navigation.navigate('Home')
                            }}
                        />

                        <Button
                            buttonStyle={[BottomButtons.button, BottomButtons.bottomButton, { borderRadius: 4, marginBottom: 0, borderColor: '#212121', borderWidth: 1 }]}
                            title="Back"
                            titleStyle={[
                                BottomButtons.bottomTitle
                            ]}
                            onPress={() => {
                                this.props.navigation.goBack();

                            }}
                        />
                    </View>
                </View>
            </View>
        )


    }

}

function mapStateToProps(state) {
    return {
        registerInput: state.user.registerInput

    };
}

const mapDispatchToProps = dispatch => {
    return {
        onRegisterInputChange: user => {
            dispatch(onRegisterInputChange(user))
        }
    };
};


class RegisterScreen extends Component {
    static navigationOptions = {
        title: "Login",
        headerStyle: {
            backgroundColor: '#212121',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    render = () => {
        return (
            <ToastContext.Consumer>
                {values => <RegisterScreenWithContext {...this.props} values={values} />}
            </ToastContext.Consumer>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen);