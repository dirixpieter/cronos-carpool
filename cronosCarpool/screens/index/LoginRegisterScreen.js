import { SplashScreen } from 'expo';
import moment from 'moment';
import React, { Component } from 'react';
import { AsyncStorage, View } from 'react-native';
import { Text } from 'react-native-elements';
import { NavigationActions, StackActions } from 'react-navigation';
import { connect } from 'react-redux';
import 'url-search-params-polyfill';
import { currentUser } from '../../actions/user-actions';
import api from '../../api';
import ToastContext from '../../classes/toast';
import LoginRegisterButtons from '../../components/LoginRegisterButtons';
import Spinner from '../../components/Spinner'
class LoginRegisterScreenWithContext extends Component {

    constructor(props) {
        super(props);
        SplashScreen.preventAutoHide();
        this.state = {
            loggedIn: true
        }
    }

    static navigationOptions = {
        title: "Login",
        headerStyle: {
            backgroundColor: '#212121',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    clearToken = async () => {
        try {
            await AsyncStorage.removeItem('access_token');
        } catch (error) {
            // Error retrieving data
        }
    }


    checkIfLoggedIn = async () => {

        try {

            const value = await AsyncStorage.getItem('access_token');

            const user = await AsyncStorage.getItem('user');
            if (value !== null && user !== null) {
                if (!(moment(value.created).add(value.ttl, 'seconds')).isAfter(moment())) {

                    let u = JSON.parse(user)
                    u = {
                        ...u,
                        access_token: JSON.parse(value)
                    }

                    this.props.currentUser(u);
                    const v = JSON.parse(value);
                    api.defaults.headers.common["Authorization"] = v.id
                    this.resetNavigation('HomeTabs');

                } else {
                    SplashScreen.hide();
                    api.defaults.headers.common["Authorization"] = null
                    this.clearToken();
                    this.setState({
                        loggedIn: false
                    })
                }



            } else {
                SplashScreen.hide();
                this.setState({
                    loggedIn: false
                })
            }
        } catch (error) {
            // Error retrieving data
        }


    }

    resetNavigation(targetRoute) {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: targetRoute }),
            ],
        });
        this.props.navigation.dispatch(resetAction);
    }



    componentDidMount() {
        SplashScreen.preventAutoHide();
        this.checkIfLoggedIn();
    }

    render() {
        return (
            <View style={{ flex: 1 }}>

                {/* <LoginHeader /> */}
                {
                    (this.state.loggedIn) ? <Spinner /> : <LoginRegisterButtons />
                }
            </View>


        );
    }
}


function mapStateToProps(state) {
    return {
        loggedIn: state.user.loggedIn

    };
}

const mapDispatchToProps = {
    currentUser,

};

class LoginRegisterScreen extends Component {
    render = () => {
        return (
            <ToastContext.Consumer>

                {values => <LoginRegisterScreenWithContext {...this.props} values={values} />}
            </ToastContext.Consumer>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginRegisterScreen);