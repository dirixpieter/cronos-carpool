import { Ionicons } from '@expo/vector-icons';
import { SecureStore } from 'expo';
import React, { Component } from 'react';
import { View } from 'react-native';
import { Button, CheckBox, Input } from 'react-native-elements';
import { NavigationActions, StackActions } from 'react-navigation';
import { connect } from 'react-redux';
import 'url-search-params-polyfill';
import { onLoginInputChange, onLoginSuccess } from '../../actions/user-actions';
import ToastContext from '../../classes/toast';
import { login } from '../../services/UserService';
import BottomButtons from '../../stylesheets/BottomButtons';
import LoginRegisterFields from '../../stylesheets/LoginRegisterFields';



class LoginScreenWithContext extends Component {
    static navigationOptions = {
        title: "Login",
        headerStyle: {
            backgroundColor: '#212121',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    constructor(props) {
        super(props);

        this.state = {
            checked: false,
            email: "",
            password: "",
        }
    }

    getCredentials = async () => {

        try {
            const email = await SecureStore.getItemAsync('email');
            const password = await SecureStore.getItemAsync('password');
            if (email !== null && password !== null) {
                const user = {
                    email: email,
                    password: password,
                };
                this.props.onLoginInputChange(user);
            }
        } catch (error) {
            // Error retrieving data
        }
    }

    loginUser() {

        const i = this.props.loginInput;
        const user = {
            email: i.email.trim().toLowerCase(),
            password: i.password.trim()
        }

        login(user)
            .then(response => {
                if (this.state.checked) {
                    SecureStore.setItemAsync("email", this.props.loginInput.email);
                    SecureStore.setItemAsync("password", this.props.loginInput.password);

                } else {
                    SecureStore.deleteItemAsync("email")
                    SecureStore.deleteItemAsync("password");
                }

                this.props.onLoginSuccess(response.data)
                this.props.onLoginInputChange({});

                this.resetNavigation('HomeTabs');
            })
            .catch(error => {
               this.props.values.toast.current.show('Login failed, make sure your password and email are correct', 1000);
                console.log("error");

            })

    }

    resetNavigation(targetRoute) {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: targetRoute }),
            ],
        });
        this.props.navigation.dispatch(resetAction);
    }

    componentDidMount = () => {

        const { params } = this.props.navigation.state
        if (typeof params !== "undefined") {
            if (typeof params.password !== "undefined") {
                const user = {
                    password: params.password,
                    email: params.returnedPerson.email
                }
                this.props.onLoginInputChange(user);
            }
        }

        this.getCredentials();
    };


    render() {
        let user = this.props.loginInput;
        return (
            <View style={{ flex: 1 }}>
                {/* <LoginHeader /> */}
                <View style={{
                    paddingLeft: 24,
                    paddingRight: 24,
                    flex: 1,
                    alignItems: "stretch"
                }}>
                    <View style={{ marginTop: 40, flex: 1, alignItems: "stretch" }}>


                        <Input
                            containerStyle={[LoginRegisterFields.input]}
                            inputContainerStyle={
                                { borderBottomWidth: 0, paddingLeft: 15 }
                            }
                            placeholder="email"
                            keyboardType='email-address'
                            onChangeText={(text) => {
                                user = {
                                    ...user,
                                    email: text
                                }
                                this.props.onLoginInputChange(user);
                            }}
                            value={this.props.loginInput.email}
                        />

                        <Input
                            containerStyle={[LoginRegisterFields.input]}
                            inputContainerStyle={
                                { borderBottomWidth: 0, paddingLeft: 15 }
                            }
                            placeholder="password"
                            onChangeText={(text) => {
                                user = {
                                    ...user,
                                    password: text
                                }
                                this.props.onLoginInputChange(user);
                            }}
                            secureTextEntry={true}
                            value={this.props.loginInput.password}
                        />

                        <CheckBox
                            title="Remember"
                            checked={this.state.checked}
                            onPress={() => this.setState({ checked: !this.state.checked })}
                            containerStyle={{
                                backgroundColor: "#FFFFFF",
                                alignSelf: 'stretch',
                                marginLeft: 0,
                                marginRight: 0,
                                margin: 0,
                                borderWidth: 0,
                                padding: 0
                            }}
                            textStyle={{
                                fontWeight: "normal"
                            }}
                            checkedIcon={<Ionicons name="md-checkbox-outline" size={24} color="#212121" />}
                            uncheckedIcon={<Ionicons name="md-square-outline" size={24} color="#212121" />}
                        />
                    </View>
                    <View style={{
                        position: "absolute", bottom: 0, left: 0, right: 0,
                        padding: 24
                    }}>
                        <Button
                            buttonStyle={[BottomButtons.button, BottomButtons.topButton, { borderRadius: 4, marginBottom: 15 }]}
                            title="Login"
                            onPress={() => {
                                if (this.props.loginInput.password != "" && this.props.loginInput.email != "") {
                                    this.loginUser();
                                }

                            }}
                        />

                        <Button
                            buttonStyle={[BottomButtons.button, BottomButtons.bottomButton, { borderRadius: 4, marginBottom: 0, borderColor: '#212121', borderWidth: 1 }]}
                            title="Back"
                            titleStyle={[
                                BottomButtons.bottomTitle
                            ]}
                            onPress={() => {
                                this.props.navigation.goBack();

                            }}
                        />
                    </View>
                </View>
            </View>


        );
    }
}

function mapStateToProps(state) {
    return {
        loginInput: state.user.loginInput,
        user: state.user.user,
        access_token: state.user.access_token

    };
}

const mapDispatchToProps = dispatch => {
    return {
        onLoginInputChange: user => {
            dispatch(onLoginInputChange(user));
        },
        onLoginSuccess: data => {
            dispatch(onLoginSuccess(data))
        }
    };
};

class LoginScreen extends Component {
    static navigationOptions = {
        title: "Login",
        headerStyle: {
            backgroundColor: '#212121',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    render = () => {
        return (
            <ToastContext.Consumer>
                {values => <LoginScreenWithContext {...this.props} values={values} />}
            </ToastContext.Consumer>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);