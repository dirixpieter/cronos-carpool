import { Picker } from 'native-base';
import React, { Component } from 'react';
import { Alert, FlatList, Picker as PickerAndroid, Platform, View, StyleSheet } from 'react-native';
import { Divider } from 'react-native-elements';
import { connect } from 'react-redux';
import { setRidesAsDriver, setRidesAsPassenger } from '../../actions/ride-actions';
import RideListItem from '../../components/RideListItem';
import { cancelRide, getRidesAsDriver, getRidesAsPassenger } from '../../services/RideService';
import Spinner from '../../components/Spinner';

class ListOfRides extends Component {

    constructor(props) {
        super(props);

        this.state = {
            refreshing: false,
            value: "upcoming",
            asDriver: true
        }
    }

    getMyRides() {
        const { asDriver } = this.state
        if (asDriver) {
            getRidesAsDriver(this.props.user.id, this.state.value)
                .then(response => {
                    this.setState({
                        refreshing: false
                    })
                    this.props.setRidesAsDriver(response.data);
                })
        } else {
            getRidesAsPassenger(this.props.user.id, this.state.value)
                .then(response => {
                    this.setState({
                        refreshing: false
                    })
                    this.props.setRidesAsPassenger(response.data);
                })
        }

    }

    async componentDidMount() {
        const { routeName } = this.props.navigation.state

        if (routeName === "Driver") {
           await this.setState({
                asDriver: true
            })
        } else if (routeName === "Passenger") {
            console.log(routeName);
            await this.setState({
                asDriver: false
            })
        }
        this.getMyRides();
    }

    handleRideItemPressed = (ride) => {
        this.props.navigation.navigate("DetailRide", { ride: ride })
    }

    handleLongPress = (ride) => {
        if (this.state.asDriver) {
            this.openAlert(ride);
        }
    }

    onRefresh() {
        this.setState({ refreshing: true }, function () { this.getMyRides() });
    }

    renderItem = ({ item }) => (
        <RideListItem
            handleOnPress={this.handleRideItemPressed}
            ride={item}
            handleLongpress={this.handleLongPress}
        />
    );

    removeRide = ride => {
        cancelRide(ride.id)
            .then(() => {
                this.onRefresh();
            })
    }
    openAlert = (ride) => {
        Alert.alert(
            'Cancel Ride',
            `Your ride to ${ride.destination["structured_formatting"]["main_text"]} will be canceled and all the passengers will be notified.`,
            [
                {
                    text: 'Back',
                },
                {
                    text: 'Cancel Ride', onPress: () => this.removeRide(ride)
                }
            ]
        )
    }

    renderPicker = () => {
        if (Platform.OS == 'android') {
            return (
                <View>
                    <PickerAndroid
                        style={[styles.picker, styles.pickerAndroid]}
                        mode='dropdown'
                        selectedValue={this.state.value}

                        onValueChange={(itemValue, itemIndex) => {
                            this.setState({
                                value: itemValue
                            })
                            this.onRefresh();
                        }}
                    >
                        <Picker.Item label="Upcoming" value={"upcoming"} />
                        <Picker.Item label="Past" value={"past"} />

                    </PickerAndroid>
                </View>
            )
        } else {
            return (
                <Picker
                    style={styles.picker}
                    mode='dropdown'
                    selectedValue={this.state.value}

                    onValueChange={(itemValue, itemIndex) => {
                        this.setState({
                            value: itemValue
                        })
                        this.onRefresh();
                    }}
                >
                    <Picker.Item label="Upcoming" value={"upcoming"} />
                    <Picker.Item label="Past" value={"past"} />

                </Picker>
            )
        }


    }


    render() {

        const { driverRides, loadingDriverRides, passengerRides, loadingPassengerRides } = this.props.ride;

        let rides = [];
        let loading = true;
        if (this.state.asDriver) {

            rides = driverRides;
            loading = loadingDriverRides
        } else {
            rides = passengerRides;
            loading = loadingPassengerRides
        }
        const list = <FlatList
            contentContainerStyle={{ padding: 24 }}
            data={rides}
            extraData={this.props.ride}
            renderItem={this.renderItem}
            keyExtractor={(item, index) => item.id}
            onRefresh={() => this.onRefresh()}
            refreshing={this.state.refreshing} />


        return (
            <View style={styles.container}>
                {this.renderPicker()}
                <Divider />
                {loading ? <Spinner /> : list}
            </View>


        );
    }
}
const offset = 16;
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    picker: {
        marginLeft: offset,
        marginRight: offset
    },
    pickerAndroid: {
        height: offset * 3
    }
})
function mapStateToProps(state) {
    return {
        ride: state.ride,
        user: state.user.user,
        location: state.location

    };
}

const mapDispatchToProps = {
    setRidesAsDriver,
    setRidesAsPassenger
};

export default connect(mapStateToProps, mapDispatchToProps)(ListOfRides);