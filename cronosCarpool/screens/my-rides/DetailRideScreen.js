import { Ionicons } from '@expo/vector-icons';
import React, { Component } from 'react';
import { FlatList, Modal, TouchableOpacity, View, ActivityIndicator, StyleSheet } from 'react-native';
import { Button, ListItem, Text } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import MapView from 'react-native-maps';
import { connect } from 'react-redux';
import { setCurrentLocation, setMarkers } from '../../actions/location-actions';
import { setPassengers } from '../../actions/ride-actions';
import { getRegionForCoordinates } from '../../classes/getRegionForCoordinates';
import ToastContext from '../../classes/toast';
import RideInfo from '../../components/RideInfo';
import TimeAndLocation from '../../components/TimeAndLocations';
import { acceptPassenger, denyPassenger, getPassengers } from '../../services/PassengerService';
import { getRide } from '../../services/RideService';
import BottomButtons from '../../stylesheets/BottomButtons';
import Shadow from '../../stylesheets/Shadow';
import Spinner from '../../components/Spinner';

class DetailRideScreenWithContext extends Component {

    static navigationOptions = {
        title: "Ride Detail",
        headerStyle: {
            backgroundColor: '#212121',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    constructor(props) {
        super(props);

        this.state = {
            ride: {},
            loading: true,
            modalVisible: false,
            modalPassenger: {
                person: {
                    name: "",
                    surname: ""
                }
            }
        }
    }

    componentDidMount = () => {
        this.getRide();


    };

    getRide = () => {
        const { navigation } = this.props;
        const { params } = navigation.state;
        let ride;
        if (params.rideId) {

            getRide(params.rideId)
                .then((response) => {
                    ride = response.data;
                    this.getPassengers(ride);
                })
                .catch((error) => {
                    this.props.values.toast.current.show('This ride was canceled', 1000);
                    this.props.navigation.goBack();
                })
        } else {
            ride = params.ride
            this.getPassengers(ride);
        }
        
    }

    getPassengers = (ride) => {
        // const url = `rides/${ride.id}/passengers?access_token=${this.props.user["access_token"].id}`

        getPassengers(ride.id)
            .then((response) => {
                this.props.setPassengers(response.data);
                this.setState({
                    modalPassenger: response.data[0],
                    ride: ride,
                    loading: false
                })
            })
            .catch((error) => {
            })
    }

    acceptPassenger = item => {
        const { navigation } = this.props;
        // const { ride } = navigation.state.params;
        const ride = this.state.ride;
        const {
            person,
            ...body
        } = item

        let object = {
            // ...body,
            passenger: item
        }
        acceptPassenger(object)
            .then((response) => {
                let p = this.props.passengers.data;
                for (let i = 0; i < p.length; i++) {
                    if (p[i].id == response.data.id) {
                        p[i].accepted = true
                    }
                }
                this.props.setPassengers(p);

            })
            .catch((error) => {
            })

    }

    denyPassenger = item => {
        const body = { data: { passenger: item } };
        denyPassenger(body)
            .then((response) => {
                this.getPassengers(this.state.ride);
            })
    }

    openModal = (passenger) => {
        this.setState({
            modalPassenger: passenger,
            modalVisible: true
        })
    }

    renderItem = ({ item }) => {

        return (
            <ListItem
                containerStyle={{ borderWidth: 1, borderColor: "#212121", borderRadius: 4 }}
                title={(item.person.id == this.props.user.id) ? item.person.name + " " + item.person.surname + " (You)" : item.person.name + " " + item.person.surname}
                rightTitle={(item.accepted) ? "accepted" : "pending"}
                onPress={() => {
                    this.openModal(item);
                }}
                onLongPress={() => {
                    if (item.person.id != this.props.user.id) {
                        this.acceptPassenger(item)

                    }
                }}
            />
        )
    }

    render() {
        const driverIsUser = this.state.ride.driverId === this.props.user.id;
        const ride = this.state.ride

        if (this.state.loading) {
            return (
                <Spinner />
            )
        } else {
            const mapOptions = getRegionForCoordinates([
                ride.start.geometry.location, ride.destination.geometry.location
            ])
            return (
                <View style={{ flex: 1 }}>
                    <MapView
                        style={{ flex: 0.5 }}
                        showsUserLocation={true}
                        initialRegion={mapOptions}
                    >
                        {/* 
                        <MapView.Marker
                            title={ride.start.description}
                            coordinate={
                                {
                                    longitude: ride.start.geometry.location.lng,
                                    latitude: ride.start.geometry.location.lat
                                }
                            }
                        /> */}

                        <MapView.Marker
                            title={ride.destination.description}
                            coordinate={
                                {
                                    longitude: ride.destination.geometry.location.lng,
                                    latitude: ride.destination.geometry.location.lat
                                }
                            }
                        />

                        <MapView.Polyline coordinates={ride.route} strokeWidth={2} strokeColor="#4286f4" />

                    </MapView>

                    <ScrollView style={{ flex: 2 }} contentContainerStyle={{ paddingLeft: 24, paddingRight: 24, paddingBottom: 24 }}>
                        <RideInfo
                            ride={this.state.ride}
                            user={this.props.user}
                            driverIsUser={driverIsUser}
                        />
                        {(this.props.passengers.data.length != 0) ? <View>
                            <Text style={{ fontSize: 18, fontWeight: 'bold', marginBottom: 16 }}>Passengers</Text>
                            <FlatList title="Passengers" data={this.props.passengers.data} extraData={this.props.passengers} renderItem={this.renderItem} keyExtractor={(item, index) => item.id} />

                        </View> : null}

                    </ScrollView>


                    {(this.state.modalPassenger != undefined) ? <Modal
                        visible={this.state.modalVisible}
                        transparent={true}
                        animationType="slide"
                        onRequestClose={() => this.setState({ modalVisible: false })}
                    >
                        <View style={[{ marginTop: 16, marginRight: 16, marginLeft: 16, backgroundColor: "white", flex: 1, padding: 24, borderRadius: 4 }, Shadow.shadow]}>
                            {/* <Button containerStyle={{ position: 'absolute', top: 16, right: 16 }} title="close" onPress={() => this.setState({ modalVisible: false })} /> */}

                            <Text style={{ color: 'black', fontSize: 18, marginBottom: 24 }}>{this.state.modalPassenger.person.name} {this.state.modalPassenger.person.surname}</Text>

                            <TimeAndLocation ride={this.state.modalPassenger} />
                            {(this.state.modalPassenger.person.id != this.props.user.id && !this.state.modalPassenger.accepted) ? <View style={[{ alignSelf: 'center' }, BottomButtons.buttonContainer]}>
                                <Button title="Accept" buttonStyle={[BottomButtons.topButton, { marginBottom: 0, }]}
                                    onPress={() => {
                                        if (this.state.modalPassenger.person.id != this.props.user.id) {
                                            this.acceptPassenger(this.state.modalPassenger)
                                            this.setState({
                                                modalVisible: false
                                            })
                                        }
                                    }} />

                                <Button title="Deny" buttonStyle={[BottomButtons.bottomButton, { borderWidth: 1, borderColor: '#212121' }]} titleStyle={BottomButtons.bottomTitle}
                                    onPress={() => {
                                        this.denyPassenger(this.state.modalPassenger);
                                        this.setState({
                                            modalVisible: false
                                        })
                                    }}
                                />
                            </View> : null}
                            <TouchableOpacity
                                style={{
                                    position: 'absolute', top: 16, right: 16,
                                    padding: 4
                                }}
                                onPress={() => {
                                    this.setState({ modalVisible: false })
                                }}>



                                <Ionicons name="md-close" size={32}
                                />
                            </TouchableOpacity>

                        </View>

                    </Modal> : null}
                </View>


            );
        }

    }
}



function mapStateToProps(state) {
    return {
        location: state.location,
        user: state.user.user,
        passengers: state.ride.passengers
    };
}

const mapDispatchToProps = dispatch => {
    return {
        setCurrentLocation: location => {
            dispatch(setCurrentLocation(location));
        },
        setMarkers: markers => {
            dispatch(setMarkers(markers));
        },
        setPassengers: passengers => {
            dispatch(setPassengers(passengers))
        }
    };
};

class DetailRideScreen extends Component {
    static navigationOptions = {
        title: "Ride Detail",
        headerStyle: {
            backgroundColor: '#212121',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    render = () => {
        return (
            <ToastContext.Consumer>
                {values => <DetailRideScreenWithContext {...this.props} values={values} />}
            </ToastContext.Consumer>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailRideScreen);