import moment from 'moment';
import React, { Component } from 'react';
import { View } from 'react-native';
import DatePicker from 'react-native-datepicker';
import { Button } from 'react-native-elements';
import MapView from 'react-native-maps';
import { connect } from 'react-redux';
import { findRideQuery, setResults } from '../../../actions/ride-actions';
import FakeInput from '../../../components/FakeInput';
import { findRides } from '../../../services/RideService';
import BottomButtons from '../../../stylesheets/BottomButtons';
import FloatingCard from '../../../stylesheets/FloatingCard';
import Shadow from '../../../stylesheets/Shadow';
import { getRegionForCoordinates } from '../../../classes/getRegionForCoordinates';
class SearchRideScreen extends Component {

    constructor(props) {
        super(props);


    }

    static navigationOptions = {
        title: "Find A Ride",
        headerStyle: {
            backgroundColor: '#212121',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    componentDidMount = () => {
        console.log("mounted");
    }

    componentWillUnmount = () => {
        this.props.findRideQuery({
            start: {
                description: "start"
            },
            destination: {
                description: "destination"
            },
            startTime: moment(),
            userId: this.props.user.id
        })
    }
    findRides = () => {
        const query = {
            ...this.props.query,
            userId: this.props.user.id
        }
        findRides(this.props.query)
            .then(response => {
                // handle success
                this.props.setResults(response.data.rides)
                this.props.navigation.navigate("SearchResults");
            })
            .catch(error => {
                // handle error
                console.log(JSON.stringify(error.response));
            })
    }

    handleDateChange(date) {
        let query = this.props.query;

        query = {
            ...query,
            "startTime": date
        }
        this.props.findRideQuery(query);
    }



    render() {

        let mapOptions;
        if ((this.props.query.start.description != "start") && (this.props.query.destination.description != "destination")) {
            mapOptions = getRegionForCoordinates([
                this.props.query.start.geometry.location, this.props.query.destination.geometry.location
            ])
        }
        else if (!this.props.location.loading) {
            mapOptions = {
                latitude: this.props.location.location.location.coords.latitude,
                longitude: this.props.location.location.location.coords.longitude,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421
            }

        }
        return (
            <View style={{ flex: 1, alignItems: 'stretch' }}>
                <MapView
                    style={{ flex: 1 }}
                    initialRegion={mapOptions}
                    showsUserLocation={true}
                >
                    {(this.props.query.start.description != "start") ? <MapView.Marker coordinate={
                        {
                            longitude: this.props.query.start.geometry.location.lng,
                            latitude: this.props.query.start.geometry.location.lat
                        }
                    } /> : null}

                    {(this.props.query.destination.description != "destination") ? <MapView.Marker coordinate={
                        {
                            longitude: this.props.query.destination.geometry.location.lng,
                            latitude: this.props.query.destination.geometry.location.lat
                        }
                    } /> : null}

                </MapView>
                <View style={[Shadow.shadow, FloatingCard.card]}>

                    <FakeInput
                        text={this.props.query.start.description}
                        isStart={true}
                        navigation={this.props.navigation}
                        pageTitle="Find A Ride"
                    />

                    <FakeInput
                        text={this.props.query.destination.description}
                        isStart={false}
                        navigation={this.props.navigation}
                        pageTitle="Find A Ride"
                    />

                    <DatePicker
                        style={{
                            width: '100%',
                            marginBottom: 24
                        }}
                        mode="datetime"
                        date={this.props.query.startTime}
                        placeholder="Time"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={{
                            dateInput: {
                                // left: 0,
                                // right: 0
                                borderWidth: 0,
                                borderBottomWidth: 1,
                                alignItems: 'flex-start'
                            },
                            dateText: {
                                marginLeft: 8
                            }
                        }}
                        showIcon={false}
                        onDateChange={(date) => this.handleDateChange(date)}
                    />
                    <Button
                        title="Search"
                        buttonStyle={[BottomButtons.topButton, { marginBottom: 0 }]}
                        onPress={() => {

                            // console.log(this.props.query);
                            if (this.props.query.start.description !== 'start' && this.props.query.destination.description !== 'destination') {
                                this.findRides();
                                //this.testFindFunction();

                            }
                        }}
                    />
                </View>
            </View >
        );
    }
}

function mapStateToProps(state) {
    return {
        query: state.ride.query,
        user: state.user.user,
        location: state.location

    };
}

const mapDispatchToProps = dispatch => {
    return {
        findRideQuery: ride => {
            dispatch(findRideQuery(ride));
        },
        setResults: rides => {
            dispatch(setResults(rides));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchRideScreen);