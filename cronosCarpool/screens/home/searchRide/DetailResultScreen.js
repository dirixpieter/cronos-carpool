import React, { Component } from 'react';
import { FlatList, ScrollView, View } from 'react-native';
import { ListItem, Text } from 'react-native-elements';
import MapView from 'react-native-maps';
import { NavigationActions, StackActions } from 'react-navigation';
import { connect } from 'react-redux';
import { setPassengers } from '../../../actions/ride-actions';
import { getRegionForCoordinates } from '../../../classes/getRegionForCoordinates';
import RideInfo from '../../../components/RideInfo';
import ResultBottomButtons from '../../../components/SearchResultBottomButtons';
import { getPassengers, requestPassenger } from '../../../services/PassengerService';

class DetailResultScreen extends Component {

    static navigationOptions = {
        title: "Ride Detail",
        headerStyle: {
            backgroundColor: '#212121',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    handleRequestPress = () => {

        console.log(this.props.navigation);
        const { ride } = this.props.navigation.state.params;

        url = `passengers/notif?access_token=${this.props.user["access_token"].id}`;
        let {
            freeSeats,
            id,
            driverId,
            ...body
        } = ride

        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({
                    routeName: 'HomeTabs',
                    action: NavigationActions.navigate({ routeName: 'DetailRide', params: { rideId: ride.id } })
                })
            ],
        });

        body = {
            ...body,
            start: this.props.query.start,
            destination: this.props.query.destination,
            personId: this.props.user.id,
            accepted: false,
            rideId: ride.id
        }

        let b = {
            passenger: body,
            driverId: ride.driverId
        }

        requestPassenger(b)
            .then((response) => {
                this.props.navigation.dispatch(resetAction);

            })
            .catch((error) => {
                console.log(JSON.stringify(error));
            })

    }



    componentDidMount = () => {
        const { navigation } = this.props;
        const { ride } = navigation.state.params;

        this.getPassengers(ride);

    };

    getPassengers = (ride) => {

        getPassengers(ride.id)
            .then((response) => {
                this.props.setPassengers(response.data);
            })
            .catch((error) => {
            })
    }

    renderItem = ({ item }) => {
        return (
            <ListItem
                containerStyle={{ borderWidth: 1, borderColor: "#212121", borderRadius: 4 }}
                title={(item.person.id == this.props.user.id) ? item.person.name + " " + item.person.surname + " (You)" : item.person.name + " " + item.person.surname}
                rightTitle={(item.accepted) ? "accepted" : "pending"}
            />
        )
    }

    checkIfalreadyRequested = () => {
        let res = false
        this.props.passengers.data.forEach(passenger => {
            if (passenger.person.id == this.props.user.id) {
                res = true
            }
        });
        return res
    }

    render() {
        const { navigation } = this.props;
        const { ride } = navigation.state.params;
        const driverIsUser = ride.driverId === this.props.user.id
        const alreadyRequested = this.checkIfalreadyRequested();
        console.log(alreadyRequested);
        const mapOptions = getRegionForCoordinates([
            ride.start.geometry.location, ride.destination.geometry.location
        ])
        return (
            <View style={{ flex: 1 }}>
                <MapView
                    style={{ flex: 0.5 }}
                    showsUserLocation={true}
                    initialRegion={mapOptions}
                >}

                    <MapView.Marker
                        title={ride.destination.description}
                        coordinate={
                            {
                                longitude: ride.destination.geometry.location.lng,
                                latitude: ride.destination.geometry.location.lat
                            }
                        }
                    />

                    <MapView.Polyline coordinates={ride.route} strokeWidth={2} strokeColor="#4286f4" />

                </MapView>
                <ScrollView style={{ flex: 2 }} contentContainerStyle={{ paddingLeft: 24, paddingRight: 24, paddingBottom: 120 }}>
                    <RideInfo
                        ride={ride}
                        user={this.props.user}
                        driverIsUser={driverIsUser}
                    />

                    {(this.props.passengers.data.length != 0) ? <View>

                        <Text style={{ fontSize: 18, fontWeight: 'bold', marginBottom: 16 }}>Passengers</Text>
                        <FlatList title="Passengers" data={this.props.passengers.data} extraData={this.props.passengers} renderItem={this.renderItem} keyExtractor={(item, index) => item.id} />
                    </View> : null}
                </ScrollView>

                {
                    (!driverIsUser && !alreadyRequested && ride.freeSeats > 0) ? <ResultBottomButtons handleRequestPress={this.handleRequestPress} /> : null
                }



            </View>


        );
    }
}



function mapStateToProps(state) {
    return {
        location: state.location,
        user: state.user.user,
        passengers: state.ride.passengers,
        query: state.ride.query
    };
}

const mapDispatchToProps = dispatch => {
    return {
        setPassengers: passengers => {
            dispatch(setPassengers(passengers))
        }
    };
};




export default connect(mapStateToProps, mapDispatchToProps)(DetailResultScreen);