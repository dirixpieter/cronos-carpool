import React, { Component } from 'react';
import { FlatList, View } from 'react-native';
import { Text } from 'react-native-elements';
import { connect } from 'react-redux';
import RideListItem from '../../../components/RideListItem';
import ToastContext from '../../../classes/toast'


class SearchResultsScreenWithContext extends Component {

    static navigationOptions = {
        headerTitle: "Results",
        headerStyle: {
            backgroundColor: '#212121',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    handleRideItemPressed = ride => {
        this.props.navigation.navigate("ResultDetail", { ride: ride })
    }

    renderItem = ({ item }) => (
        <RideListItem
            handleOnPress={this.handleRideItemPressed}
            ride={item}
        />
    );
    //this.props.values.toast.current.show('No rides found', 1500)

    componentDidMount = () => {
        (this.props.results.length == 0) ? this.props.values.toast.current.show('No rides found', 1500) : null
    };

    render() {

        const results = <FlatList contentContainerStyle={{ padding: 24 }} data={this.props.results} extraData={this.props.ride} renderItem={this.renderItem} keyExtractor={(item, index) => item.id} />


        return (
            <View style={{ flex: 1, alignItems: 'stretch' }}>
                {(this.props.results.length != 0) ? results : null}
            </View>


        );
    }
}

function mapStateToProps(state) {
    return {
        ride: state.ride,
        results: state.ride.results,
        user: state.user.user

    };
}

const mapDispatchToProps = {

};

class SearchResultsScreen extends Component {
    static navigationOptions = {
        title: "Results",
        headerStyle: {
            backgroundColor: '#212121',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    render = () => {
        return (
            <ToastContext.Consumer>
                {values => <SearchResultsScreenWithContext {...this.props} values={values} />}
            </ToastContext.Consumer>
        )
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(SearchResultsScreen);