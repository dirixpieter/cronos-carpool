import React, { Component } from 'react';
import { View } from 'react-native';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';
import { findRideQuery, onRideInputChange, setResults, setRidesAsDriver } from '../../actions/ride-actions';
import GooglePlaceInput from '../../components/GooglePlaceInput';
import BottomButtons from '../../stylesheets/BottomButtons';

class LocationInputScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            start: "",
            destination: ""
        }
    }

    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title', 'Find A Ride'),
            headerStyle: {
                backgroundColor: '#212121',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        };
    }

    componentWillUnmount = () => {
      console.log("unmounted");
    };
    
    _handleLocationPress = (object) => {
        let query = this.props.query

        if (object.isStart) {
            query = {
                ...query,
                start: object
            }
        } else {
            query = {
                ...query,
                destination: object
            }
        }

        this.props.findRideQuery(query);
    }

    componentDidMount = () => {
    };

    
    _handleLocationPressOffer = (object) => {
        let ride = this.props.newRide;

        if (object.isStart) {
            ride = {
                ...ride,
                start: object
            }
        } else {
            ride = {
                ...ride,
                destination: object
            }
        }

        this.props.onRideInputChange(ride);
    }

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'stretch' }}>
                <GooglePlaceInput
                    placeholder={(this.props.navigation.getParam('isStart')) ? "start" : "destination"}
                    isStart={this.props.navigation.getParam('isStart')}
                    handleOnPress={(this.props.navigation.state.params.title === 'Offer A Ride') ? this._handleLocationPressOffer : this._handleLocationPress}
                />

                <Button
                    buttonStyle={[BottomButtons.topButton, BottomButtons.buttonContainer, {marginBottom:0}]}
                    title="Next"
                    onPress={() => {
                        if (this.props.navigation.state.params.title === 'Offer A Ride') {
                            this.props.navigation.navigate('AddRide')
                        } else {
                            this.props.navigation.navigate('SearchRide', { locs: this.state})
                        }

                    }}
                />
            </View>


        );
    }
}

function mapStateToProps(state) {
    return {
        query: state.ride.query,
        user: state.user.user,
        newRide: state.ride.newRide,
        location: state.location,
        driverRides: state.ride.driverRides

    };
}

const mapDispatchToProps = dispatch => {
    return {
        findRideQuery: ride => {
            dispatch(findRideQuery(ride));
        },
        setResults: rides => {
            dispatch(setResults(rides));
        },
        onRideInputChange: ride => {
            dispatch(onRideInputChange(ride));
        },
        setRidesAsDriver: rides => {
            dispatch(setRidesAsDriver(rides))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LocationInputScreen);