import moment from 'moment';
import React, { Component } from 'react';
import { KeyboardAvoidingView, TextInput, View, StyleSheet, Platform } from 'react-native';
import { Picker } from 'native-base'
import { Button, Slider, Text } from 'react-native-elements';
import { ScrollView, Switch } from 'react-native-gesture-handler';
import { NavigationActions, StackActions } from 'react-navigation';
import { connect } from 'react-redux';
import { onNewRoundTrip, onRideInputChange, setRidesAsDriver } from '../../../actions/ride-actions';
import { setCars } from '../../../actions/user-actions';
import Days from '../../../classes/days';
import { getRideInfo } from '../../../classes/GetRouteInformation';
import ToastContext from '../../../classes/toast';
import DateTimeSelect from '../../../components/DateTimeSelect';
import FakeInput from '../../../components/FakeInput';
import { addRide } from '../../../services/RideService';
import BottomButtons from '../../../stylesheets/BottomButtons';



class AddRideFormWithContext extends Component {

    constructor(props) {
        super(props);

        this.state = {
            date: props.newRide.startTime,
            value: props.cars[0],
            roundTrip: false,
            departArrival: true,
        }
    }


    static navigationOptions = {
        title: "Offer A Ride",
        headerStyle: {
            backgroundColor: '#212121',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };



    refreshList = (data) => {
        const d = this.props.driverRides;
        let r;
        this.props.onRideInputChange({
            start: {
                "description": "start",
            },
            destination: {
                "description": "destination",
            },
            startTime: moment(),
            freeSeats: 1,
            driverId: "",
            departAt: true

        });

        this.props.onNewRoundTrip({
            start: {
                "description": "start",
            },
            destination: {
                "description": "destination",
            },
            startTime: moment(),
            freeSeats: 1,
            driverId: "",
            departAt: true,
            extra: ""

        });
        if (this.state.roundTrip) {
            d.push(...data);
        } else {
            d.push(data);
        }
        this.props.setRidesAsDriver(d);

        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({
                    routeName: 'HomeTabs',
                    action: NavigationActions.navigate({ routeName: 'My Rides' })
                })
            ],
        });


        this.props.navigation.dispatch(resetAction);

    };

    componentDidMount = () => {
        this.setState({
            value: this.props.cars[0]
        })
        this.setHighlightedDay(this.props.newRide.startTime);
    };

    setHighlightedDay = date => {
        const dayOfTheWeek = Days[moment(date).isoWeekday() - 1];

        this.setState({ bulk: { [dayOfTheWeek]: true }, currentDay: dayOfTheWeek })
    }



    async addRide() {



        let body;
        let ride = this.props.newRide;
        ride = {
            ...ride,
            driverId: this.props.user.id,
            carInfo: this.state.value
        }



        const rideInfo = await getRideInfo(ride);
        ride = {
            ...ride,
            rideTime: rideInfo.duration.value,
            distance: rideInfo.distance.value,
            route: rideInfo.decodedPolyline
        };

        if (this.state.roundTrip) {

            let roundTrip = this.props.newRoundTrip;
            roundTrip = {
                ...roundTrip,
                driverId: this.props.user.id,
                start: ride.destination,
                destination: ride.start,
                carInfo: this.state.value
            }

            const roundInfo = await getRideInfo(roundTrip);

            roundTrip = {
                ...roundTrip,
                rideTime: roundInfo.duration.value,
                distance: roundInfo.distance.value,
                route: roundInfo.decodedPolyline
            };


            body = [ride, roundTrip];
        } else {


            body = ride
        }
        addRide(body)
            .then((response) => {
                this.refreshList(response.data)
            })
            .catch((error) => {
                console.log(JSON.stringify(error.response));
            });
    }

    handleDateChange = (date, isRoundTrip, departArrival) => {
        if (isRoundTrip) {
            let r = this.props.newRoundTrip;
            if (!moment(date).isAfter(this.props.newRide.startTime)) {

                r = {
                    ...r,
                    departAt: departArrival,
                    startTime: this.props.newRide.startTime
                }
                this.props.values.toast.current.show("The ride back's date should be later than the first ride", 1500);
            } else {
                r = {
                    ...r,
                    departAt: departArrival,
                    startTime: date
                }

            }
            this.props.onNewRoundTrip(r);
        } else if (!isRoundTrip) {
            let r = this.props.newRide;
            r = {
                ...r,
                departAt: departArrival,
                startTime: date
            }
            this.props.onRideInputChange(r);
            this.setHighlightedDay(date);
        }
    }


    

    render() {

        let ride = this.props.newRide;

        return (
            <View style={{ flex: 1, alignItems: 'stretch' }}>

                <ScrollView style={styles.scrollView}>
                    <KeyboardAvoidingView behavior="position" enabled>
                        <Text h4
                            style={styles.margin}
                        >Location and time</Text>
                        <FakeInput
                            text={this.props.newRide.start.description}
                            isStart={true}
                            navigation={this.props.navigation}
                            pageTitle="Offer A Ride"
                        />

                        <FakeInput
                            text={this.props.newRide.destination.description}
                            isStart={false}
                            navigation={this.props.navigation}
                            pageTitle="Offer A Ride"
                        />

                        <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between', marginBottom: 16 }}>
                            <Text>Round trip </Text>
                            <Switch
                                onValueChange={(value) => this.setState({ roundTrip: value })}
                                value={this.state.roundTrip}
                                thumbColor="#4286f4"
                                trackColor={{
                                    false: "lightgrey",
                                    true: '#80b5ff'
                                }}
                            />
                        </View>

                        <DateTimeSelect date={this.props.newRide.startTime} isRoundTrip={false} handleDateChange={this.handleDateChange} />

                        {
                            (this.state.roundTrip) ? <DateTimeSelect date={this.props.newRoundTrip.startTime} isRoundTrip={true} handleDateChange={this.handleDateChange} /> : null
                        }

                        <Text h4
                            style={styles.margin}
                        >Details</Text>
                        <Text h5 style={styles.title}>Number of free seats </Text>
                        <View style={styles.carPicker}>
                            <Picker
                                onValueChange={(itemValue, itemIndex) => {
                                    console.log(itemValue);
                                    this.setState({
                                        value: itemValue
                                    })
                                    ride = {
                                        ...ride,
                                        freeSeats: 1
                                    }
                                    this.props.onRideInputChange(ride);
                                }}
                                mode='dropdown'
                                selectedValue={this.state.value}
                                itemStyle={{
                                    padding: 0
                                }}
                                style={{

                                    paddingLeft: 0,
                                }}
                            >

                                {this.props.cars.map((car, index) => {
                                    console.log(car.id);
                                    return <Picker.Item key={index} value={car} label={car.model} />
                                })}

                            </Picker>
                        </View>

                        <Text style={styles.freeSeats} >{this.props.newRide.freeSeats} out of {this.state.value.freeSeats}</Text>
                        <Slider
                            step={1}
                            maximumValue={this.state.value.freeSeats}
                            minimumValue={1}
                            value={this.props.newRide.freeSeats}
                            animationType='timing'
                            thumbTintColor="#4286f4"
                            minimumTrackTintColor="#4286f4"
                            style={styles.margin}
                            onValueChange={(text) => {
                                ride = {
                                    ...ride,
                                    freeSeats: parseInt(text)
                                }
                                this.props.onRideInputChange(ride);
                            }}
                        />


                        <Text h5 style={styles.title}>Extra</Text>
                        <TextInput
                            placeholder="Extra info"
                            multiline={true}
                            numberOfLines={Platform.OS === 'ios' ? null : 6}
                            minHeight={(Platform.OS === 'ios' && 6) ? (20 * 6) : null}
                            style={styles.extra}
                            onChangeText={(text) => {
                                ride = {
                                    ...ride,
                                    extra: text
                                }
                                this.props.onRideInputChange(ride);
                            }}
                        />
                        <Button
                            buttonStyle={[BottomButtons.topButton, { marginBottom: offset * 3 }]}
                            title="Add Ride(s)"
                            onPress={() => {
                                const r = this.props.newRide;
                                if (r.start.description != "start" && r.destination.description != "destination") {
                                    this.addRide();
                                } else {
                                    this.props.values.toast.current.show("The start and destination cannot be blank", 1500);
                                }

                            }}
                        />
                    </KeyboardAvoidingView>
                </ScrollView>

            </View>
        );
    }
}
const offset = 16
const styles = StyleSheet.create({
    title: {
        marginBottom: offset,
        fontWeight: 'bold',
        fontSize: 18
    },
    scrollView: {
        flex: 1,
        padding: offset * 1.5,
        paddingBottom: offset * 3
    },
    extra: {
        textAlignVertical: "top",
        borderWidth: 1,
        borderColor: '#000000',
        padding: 6,
        marginBottom: offset * 1.5,
    },
    margin: {
        marginBottom: 16
    },
    freeSeats: {
        marginBottom: offset / 2
    },
    carPicker: {
        borderWidth: 1, borderColor: '#DDDDDD',
        marginBottom: 16,
    }

})

function mapStateToProps(state) {
    return {
        newRide: state.ride.newRide,
        newRoundTrip: state.ride.newRoundTrip,
        user: state.user.user,
        location: state.location,
        driverRides: state.ride.driverRides,
        cars: state.user.cars


    };
}

const mapDispatchToProps = dispatch => {
    return {
        onRideInputChange: ride => {
            dispatch(onRideInputChange(ride));
        },
        setRidesAsDriver: rides => {
            dispatch(setRidesAsDriver(rides))
        },
        onNewRoundTrip: ride => {
            dispatch(onNewRoundTrip(ride))
        },
        setCars: cars => {
            dispatch(setCars(cars))
        }
    };
};

class AddRideForm extends Component {
    static navigationOptions = {
        title: "Offer A Ride",
        headerStyle: {
            backgroundColor: '#212121',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    render = () => {
        return (
            <ToastContext.Consumer>
                {values => <AddRideFormWithContext {...this.props} values={values} />}
            </ToastContext.Consumer>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddRideForm);