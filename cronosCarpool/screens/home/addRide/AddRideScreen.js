import React, { Component } from 'react';
import { View } from 'react-native';
import { Button } from 'react-native-elements';
import MapView from 'react-native-maps';
import { connect } from 'react-redux';
import { onRideInputChange, setRidesAsDriver } from '../../../actions/ride-actions';
import { setCars } from '../../../actions/user-actions';
import { getRegionForCoordinates } from '../../../classes/getRegionForCoordinates';
import FakeInput from '../../../components/FakeInput';
import { getCars } from '../../../services/CarService';
import BottomButtons from '../../../stylesheets/BottomButtons';
import FloatingCard from '../../../stylesheets/FloatingCard';
import Shadow from '../../../stylesheets/Shadow';
class AddRideScreen extends Component {



    static navigationOptions = {
        title: "Offer A Ride",
        headerStyle: {
            backgroundColor: '#212121',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    constructor(props) {
        super(props);

        this.state = {
            nextRoute: 'AddRideForm'
        }
    }



    componentWillUnmount = () => {
        this.props.onRideInputChange({
            start: {
                "description": "start",
            },
            destination: {
                "description": "destination",
            }
        })
    }

    _handleLocationPress = (object) => {
        let ride = this.props.newRide;



        if (object.isStart) {
            ride = {
                ...ride,
                start: object
            }
        } else {
            ride = {
                ...ride,
                destination: object
            }
        }

        this.props.onRideInputChange(ride);
    }

    renderPolyLine = () => {
        let ride = this.props.newRide;


    }

    getCars = async () => {
        await getCars(this.props.user.id)
            .then(async (response) => {

                if (response.data.length == 0) {
                    this.setState({
                        nextRoute: 'AddCar',
                    })
                }

                await this.props.setCars(response.data);
                this.props.navigation.navigate(this.state.nextRoute, { nextRoute: 'AddRideForm' })
            })
    }

    componentDidMount = () => {

    };

    next = () => {
        this.getCars();

    }

    render() {

        let mapOptions;
        if ((this.props.newRide.start.description != "start") && (this.props.newRide.destination.description != "destination")) {
            mapOptions = getRegionForCoordinates([
                this.props.newRide.start.geometry.location, this.props.newRide.destination.geometry.location
            ])
        }
        else if (!this.props.location.loading) {
            mapOptions = {
                latitude: this.props.location.location.location.coords.latitude,
                longitude: this.props.location.location.location.coords.longitude,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421
            }

        }
        return (
            <View style={{ flex: 1, alignItems: 'stretch' }}>
                <MapView
                    style={{ flex: 1 }}
                    initialRegion={mapOptions}
                    showsUserLocation={true}
                >
                    {(this.props.newRide.start.description != "start") ? <MapView.Marker coordinate={
                        {
                            longitude: this.props.newRide.start.geometry.location.lng,
                            latitude: this.props.newRide.start.geometry.location.lat
                        }
                    } /> : null}

                    {(this.props.newRide.destination.description != "destination") ? <MapView.Marker coordinate={
                        {
                            longitude: this.props.newRide.destination.geometry.location.lng,
                            latitude: this.props.newRide.destination.geometry.location.lat
                        }
                    } /> : null}


                </MapView>
                <View style={[Shadow.shadow, FloatingCard.card]}>

                    <FakeInput
                        text={this.props.newRide.start.description}
                        isStart={true}
                        navigation={this.props.navigation}
                        pageTitle="Offer A Ride"
                    />

                    <FakeInput
                        text={this.props.newRide.destination.description}
                        isStart={false}
                        navigation={this.props.navigation}
                        pageTitle="Offer A Ride"
                    />

                    <Button
                        title="Next"
                        buttonStyle={[BottomButtons.topButton, { marginBottom: 0 }]}
                        onPress={() => {
                            //if (this.props.query.where["start.description"] !== 'start' && this.props.query.where["destination.description"] !== 'destination') {
                            this.next();

                            //}
                        }}
                    />
                </View>
            </View>

        );
    }
}

function mapStateToProps(state) {
    return {
        newRide: state.ride.newRide,
        user: state.user.user,
        location: state.location,
        driverRides: state.ride.driverRides

    };
}

const mapDispatchToProps = dispatch => {
    return {
        onRideInputChange: ride => {
            dispatch(onRideInputChange(ride));
        },
        setRidesAsDriver: rides => {
            dispatch(setRidesAsDriver(rides))
        },
        setCars: cars => {
            dispatch(setCars(cars))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddRideScreen);