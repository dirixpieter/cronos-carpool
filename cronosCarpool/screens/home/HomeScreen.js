import { Constants, Location, Notifications, Permissions, SplashScreen } from 'expo';
import React, { Component } from 'react';
import { Platform, StatusBar, View } from 'react-native';
import { Button } from 'react-native-elements';
import MapView from 'react-native-maps';
import { StackActions } from 'react-navigation';
import { connect } from 'react-redux';
import { setCurrentLocation } from '../../actions/location-actions';
import { nrOfPassengers, setRidesAsDriver, setRidesAsPassenger } from '../../actions/ride-actions';
import { newNotif, onLoginSuccess, addNotifToken } from '../../actions/user-actions';
import { registerForPushNotificationsAsync } from '../../services/NotificationService';
import { getRidesAsDriver, getRidesAsPassenger } from '../../services/RideService';
import BottomButtons from '../../stylesheets/BottomButtons';
import Shadow from '../../stylesheets/Shadow';


class HomeScreen extends Component {

    static navigationOptions = {
        title: "Cronos Carpool",
        headerStyle: {
            backgroundColor: '#212121',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    async  componentDidMount() {


        if (Platform.OS === 'android' && !Constants.isDevice) {
            SplashScreen.hide();
            this.setState({
                errorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
            });
        } else {
            this._getLocationAsync();
        }

        registerForPushNotificationsAsync(this.props.user)
        
        this._notificationSubscription = Notifications.addListener(this._handleNotification);
    }


    _handleNotification = (notification) => {
        switch (notification.origin) {
            case 'received':
                this.props.newNotif(true)
                break;
            case 'selected':
                this.props.newNotif(false)
                if (notification.data.rideId) {
                    this.getMyRides();
                    this.props.navigation.dispatch(StackActions.popToTop());
                    this.props.navigation.navigate("DetailRide", { "rideId": notification.data.rideId });
                }
                if (notification.data.denied) {

                    this.getMyRides();
                    this.props.navigation.dispatch(StackActions.popToTop());
                }

                if (notification.data.canceled) {
                    this.getMyRides();
                    this.props.navigation.dispatch(StackActions.popToTop());
                    this.props.navigation.navigate("Notifications");;
                }

                break

            default:
                break;
        }

    }

    getMyRides() {

        getRidesAsPassenger(this.props.user.id, "upcoming")
            .then(response => {
                this.props.setRidesAsPassenger(response.data);
            })
            .catch(error => {
                // handle error
                console.log(JSON.stringify(error.response));
            })
        getRidesAsDriver(this.props.user.id, "upcoming")
            .then(response => {
                this.props.setRidesAsDriver(response.data);
            })
            .catch(error => {
                // handle error
                console.log(JSON.stringify(error.response));
            })
    }

    _getLocationAsync = async () => {

        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
            this.setState({
                errorMessage: 'Permission to access location was denied',
            });
        }

        let location = await Location.getCurrentPositionAsync({});
        this.props.setCurrentLocation({ location });
        SplashScreen.hide();
    };



    render() {
        let mapOptions;

        if (!this.props.location.loading) {
            mapOptions = {
                latitude: this.props.location.location.location.coords.latitude,
                longitude: this.props.location.location.location.coords.longitude,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421
            }

        }
        return (
            <View style={{ flex: 1 }}>
                <StatusBar
                    barStyle="light-content"
                    backgroundColor="#FFFFFF"
                />
                <MapView
                    style={{ flex: 1 }}
                    initialRegion={mapOptions}
                    showsUserLocation={true}
                >

                </MapView>
                <View style={[BottomButtons.buttonContainer]}>
                    <Button
                        buttonStyle={[BottomButtons.topButton, Shadow.shadow, {
                            marginBottom: 15,
                            backgroundColor: "#212121"
                        }]}
                        title="Offer A Ride"
                        onPress={() => {
                            this.props.navigation.navigate('AddRide')
                        }}
                    />
                    <Button
                        buttonStyle={[BottomButtons.bottomButton, Shadow.shadow, {
                            backgroundColor: "#fff"

                        }]}
                        titleStyle={BottomButtons.bottomTitle}
                        title="Find A Ride"
                        onPress={() => {
                            this.props.navigation.navigate('SearchRide')
                        }}
                    />
                </View>
            </View>


        );
    }
}



function mapStateToProps(state) {
    return {
        location: state.location,
        user: state.user.user,
        passengerRides: state.ride.passengerRides,
        nrOfPassengers: state.ride.nrOfPassengers,

    };
}

const mapDispatchToProps = dispatch => {
    return {
        setCurrentLocation: location => {
            dispatch(setCurrentLocation(location));
        },
        nrOfPassengers: nr => {
            dispatch(nrOfPassengers(nr));
        },
        setRidesAsDriver: nr => {
            dispatch(setRidesAsDriver(nr));
        },
        onLoginSuccess: user => {
            dispatch(onLoginSuccess(user));
        },
        setRidesAsPassenger: passengers => {
            dispatch(setRidesAsPassenger(passengers));
        },
        newNotif: notif => {
            dispatch(newNotif(notif));
        },
        addNotifToken: token => {
            dispatch(addNotifToken(token))
        }
    };
};




export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);