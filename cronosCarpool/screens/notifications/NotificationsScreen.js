import moment from 'moment';
import React, { Component } from 'react';
import { FlatList, View } from 'react-native';
import { connect } from 'react-redux';
import { newNotif, setNotifications } from '../../actions/user-actions';
import NotificationListItem from '../../components/NotificationListItem';
import { getNotifications, removeNotification } from '../../services/NotificationService';

class NotificationsScreen extends Component {

    static navigationOptions = {
        title: "Notifications",
        headerStyle: {
            backgroundColor: '#212121',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    constructor(props) {
        super(props);

        this.state = {
            refreshing: false
        }
    }

    onRefresh() {
        this.setState({ refreshing: true }, function () { this.getMyNotifications() });
    }

    //people/5cac986a06a3440bac733ebc/notifications?
    componentDidMount = () => {
        this.getMyNotifications();
    }

    getMyNotifications() {


        getNotifications(this.props.user.id)
            .then(response => {
                const sortedArray = response.data.sort((a, b) => moment(b.startTime) - moment(a.startTime))
                this.setState({ refreshing: false })
                this.props.setNotifications(sortedArray.reverse());
            })
            .catch(error => {
                // handle error
                console.log(JSON.stringify(error.response));
            })
    }
    //moment(ride.startTime).tz('Europe/Brussels').format('HH:mm')

    removeNotification = id => {
        removeNotification(this.props.user.id, id)
            .then(() => this.onRefresh());
    }

    handleOnLongPress = notif => {
        this.removeNotification(notif.id);
    }

    handleOnPress = (notif) => {
        const data = notif.data;
        this.props.newNotif(false);

        if (data.rideId !== undefined) {
            this.props.navigation.navigate('DetailRide', { rideId: data.rideId });
        } else if (data.denied !== undefined) {
            return;
        }
    }
    renderItem = ({ item }) => (
        <NotificationListItem
            notif={item}
            handleOnPress={this.handleOnPress}
            handleOnLongPress={this.handleOnLongPress}
        />
        // <ListItem
        //     title={item.title}
        //     subtitle={item.body}
        //     rightTitle={
        //         moment(item.sendDate).format('llll')
        //     }
        //     onPress={() => {
        //         const data = item.data;
        //         this.removeNotification(item.id);

        //         if (data.rideId !== undefined) {
        //             this.props.navigation.navigate('DetailRide', { rideId: data.rideId });
        //         } else if (data.denied !== undefined) {
        //             return;
        //         }
        //     }}
        // />
    )
    render() {

        const notificationList = <FlatList
            contentContainerStyle={{ padding: 24, flex: 1 }}
            data={this.props.notifications}
            extraData={this.props.notifications}
            renderItem={this.renderItem}
            keyExtractor={(item, index) => item.id}
            onRefresh={() => this.onRefresh()}
            refreshing={this.state.refreshing} />

        return (
            <View style={{ flex: 1, alignItems: 'stretch' }}>
                {(this.props.loading) ? null : notificationList}
            </View>


        );
    }
}

function mapStateToProps(state) {
    return {
        ride: state.ride,
        user: state.user.user,
        notifications: state.notifications.notifications,
        loading: state.notifications.loadingNotifs,

    };
}

const mapDispatchToProps = {
    setNotifications,
    newNotif
};

export default connect(mapStateToProps, mapDispatchToProps)(NotificationsScreen);


