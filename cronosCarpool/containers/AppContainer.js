import { Ionicons } from '@expo/vector-icons';
import React from 'react';
import { createAppContainer, createBottomTabNavigator, createMaterialTopTabNavigator, createStackNavigator } from "react-navigation";
import ListOfRides from '../screens/my-rides/ListOfRides';
import NotificationIcon from '../components/NotificationIcon';
import AddCarScreen from "../screens/AddCarScreen";
import AddRideForm from "../screens/home/addRide/AddRideForm";
import AddRideScreen from "../screens/home/addRide/AddRideScreen";
import HomeScreen from "../screens/home/HomeScreen";
import LocationInputScreen from "../screens/home/LocationInputScreen";
import DetailResultScreen from "../screens/home/searchRide/DetailResultScreen";
import SearchResultsScreen from "../screens/home/searchRide/SearchResultsScreen";
import SearchRideScreen from "../screens/home/searchRide/SearchRideScreen";
import LoginRegisterScreen from "../screens/index/LoginRegisterScreen";
import LoginScreen from "../screens/index/LoginScreen";
import RegisterScreen from "../screens/index/RegisterScreen";
import DetailRideScreen from "../screens/my-rides/DetailRideScreen";
import NotificationsScreen from "../screens/notifications/NotificationsScreen";
import CarScreen from "../screens/profile/CarScreen";
import EditProfileScreen from "../screens/profile/EditProfileScreen";
import ProfileScreen from "../screens/profile/ProfileScreen";
import SettingsScreen from "../screens/profile/SettingsScreen";

const MyRides = createMaterialTopTabNavigator(
    {
        Driver: {
            screen: ListOfRides,
            navigationOptions: () => ({
                headerBackTitle: null,
            }),

        },
        Passenger: {
            screen: ListOfRides,
            navigationOptions: () => ({
                headerBackTitle: null,
            }),

        }
    },
    {
        lazy: true,
        tabBarOptions: {
            style: {
                backgroundColor: '#212121',
            },
            indicatorStyle: {
                backgroundColor: "#4286f4",
            },

        }
    }

)


const MyRidesTabs = createStackNavigator(
    {
        Rides: {
            screen: MyRides,
            navigationOptions: () => ({
                headerBackTitle: null,
            }),

        },
        DetailRide: {
            screen: DetailRideScreen,
            navigationOptions: () => ({
                headerBackTitle: null,
            }),

        }
    },
    {
        initialRouteName: "Rides",
        defaultNavigationOptions: {
            headerTitle: "My Rides",
            headerStyle: {
                backgroundColor: '#212121',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        },
    }

)

const NotificationsStack = createStackNavigator(
    {
        Notifications: {
            screen: NotificationsScreen,
            navigationOptions: () => ({
                headerBackTitle: null,
            }),

        }
    },
    {
        initialRouteName: "Notifications"
    }
)

const ProfileStack = createStackNavigator(
    {
        Profile: {
            screen: ProfileScreen,
            navigationOptions: () => ({
                headerBackTitle: null,
            }),

        },
        EditProfile: {
            screen: EditProfileScreen,
            navigationOptions: () => ({
                headerBackTitle: null,
            }),

        },
        Settings: {
            screen: SettingsScreen,
            navigationOptions: () => ({
                headerBackTitle: null,
            }),

        },
        Cars: {
            screen: CarScreen,
            navigationOptions: () => ({
                headerBackTitle: null,
            }),

        },
        AddCar: {
            screen: AddCarScreen,
            navigationOptions: () => ({
                headerBackTitle: null,
            }),

        },
    },
    {
        initialRouteName: "Profile"
    }
)



const HomeStack = createStackNavigator(
    {
        Home: {
            screen: HomeScreen,
            navigationOptions: () => ({
                headerBackTitle: null,
            }),

        },
        AddRide: {
            screen: AddRideScreen,
            navigationOptions: () => ({
                headerBackTitle: null,
            }),

        },
        AddRideForm: {
            screen: AddRideForm,
            navigationOptions: () => ({
                headerBackTitle: null,
            }),

        },
        SearchRide: {
            screen: SearchRideScreen,
            navigationOptions: () => ({
                headerBackTitle: null,
            }),

        },
        LocationInput: {
            screen: LocationInputScreen,
            navigationOptions: () => ({
                headerBackTitle: null,
            }),

        },
        SearchResults: {
            screen: SearchResultsScreen,
            navigationOptions: () => ({
                headerBackTitle: null,
            }),

        },
        ResultDetail: {
            screen: DetailResultScreen,
            navigationOptions: () => ({
                headerBackTitle: null,
            }),

        },
        AddCar: {
            screen: AddCarScreen,
            navigationOptions: () => ({
                headerBackTitle: null,
            }),

        }

    },
    {
        initialRouteName: "Home"
    }

)

const HomeTabs = createBottomTabNavigator(
    {
        Home: {
            screen: HomeStack,
            navigationOptions: () => ({
                headerBackTitle: null,
            }),

        },
        "My Rides": {
            screen: MyRidesTabs,
            navigationOptions: () => ({
                headerBackTitle: null,
            }),

        },
        Notifications: {
            screen: NotificationsStack,
            navigationOptions: () => ({
                headerBackTitle: null,
            }),

        },
        Profile: {
            screen: ProfileStack,
            navigationOptions: () => ({
                headerBackTitle: null,
            }),

        }
    },

    {
        lazy: true,
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                const { routeName } = navigation.state;
                let IconComponent = Ionicons;
                let iconName;
                if (routeName === 'Home') {
                    iconName = `md-home`;

                } else if (routeName === 'My Rides') {
                    iconName = `md-list`;
                } else if (routeName === 'Notifications') {
                    return <NotificationIcon tintColor={tintColor} />

                } else if (routeName === 'Profile') {
                    iconName = `md-person`;
                }

                // You can return any component that you like here!
                return <IconComponent name={iconName} size={25} color={tintColor} />;
            },
        })
        ,
        tabBarOptions: {
            style: {
                backgroundColor: '#212121',
            },
            showLabel: false,
            activeTintColor: "#ffffff",

        },
    }
)

const AppNavigator = createStackNavigator(
    {
        LoginRegister: {
            screen: LoginRegisterScreen,
            navigationOptions: () => ({
                headerBackTitle: null,
                title: "Cronos Carpool",
                headerStyle: {
                    backgroundColor: '#212121',
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                }
            }),

        },
        Login: {
            screen: LoginScreen,
            navigationOptions: () => ({
                title: "Login",
                headerStyle: {
                    backgroundColor: '#212121',
                },
                headerTintColor: '#fff',
                headerBackTitle: null,
                headerTitleStyle: {
                    fontWeight: 'bold',
                }
            }),
        },
        Register: {
            screen: RegisterScreen,
            navigationOptions: () => ({
                title: "Register",
                headerStyle: {
                    backgroundColor: '#212121',
                },
                headerTintColor: '#fff',
                headerBackTitle: null,
                headerTitleStyle: {
                    fontWeight: 'bold',
                }
            }),
        },
        AddCar: {
            screen: AddCarScreen,
            navigationOptions: () => ({
                title: "Add a car",
                headerStyle: {
                    backgroundColor: '#212121',
                },
                headerTintColor: '#fff',
                headerBackTitle: null,
                headerTitleStyle: {
                    fontWeight: 'bold',

                }
            }),
        },
        HomeTabs: {
            screen: HomeTabs,
            navigationOptions: () => ({
                title: 'Cronos Carpool',
                header: null,
                headerBackTitle: null,
            })
        }
    },
    {
        initialRouteName: "LoginRegister",

    }

)



export default createAppContainer(AppNavigator);