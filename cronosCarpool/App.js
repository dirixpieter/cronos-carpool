import { SplashScreen } from 'expo';
import React from 'react';
import Toast from 'react-native-easy-toast';
import { Provider } from 'react-redux';
import ToastContext from './classes/toast';
import AppContainer from './containers/AppContainer';
import store from './store';
class App extends React.Component {
  componentDidMount = () => {
    
  };

  constructor(props) {
    super(props);
    SplashScreen.preventAutoHide();
    this.toast = React.createRef();
    this.state = {
    }
  }
  render() {

    const toast = this.toast;
    const values = {
      toast,
    }

    return (
      <Provider store={store}>
        <ToastContext.Provider value={values}>


          <AppContainer />
          <Toast
            ref={toast}
            positionValue={120}
          />
        </ToastContext.Provider>
      </Provider>
    );
  }
}






export default App;