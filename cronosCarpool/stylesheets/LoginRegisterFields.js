import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    input: {
        borderRadius: 4,
        borderWidth: 1,
        borderColor: '#212121',
        padding: 3,
        marginBottom: 24
        
    }
});