import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    card: { 
        position: "absolute",
        bottom: 24, left: 24, right: 24,
        flex: 1, 
        flexDirection: "column", 
        alignItems: 'stretch', 
        backgroundColor: 'white', 
        padding: 15, 
        borderRadius: 4,
         
    }
});