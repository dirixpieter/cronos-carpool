import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    buttonContainer: {
        position: "absolute",
        bottom: 24, left: 24, right: 24
    },
    topButton: {
        padding: 12.5,
        marginBottom: 15,
        backgroundColor: '#212121'
    },
    topTitle: {
        color: '#FFFFFF'
    },
    bottomButton: {
        padding: 12.5,
        backgroundColor: '#FFFFFF',
    },
    bottomTitle : {
        color: '#212121'
    }
});