import api from "../api";



export const login = (body) => {

    return api.post(`people/login?include=user`, body)
}

export const register = (body) => {
    return api.post(`people`, body);
    
}
export const update = (userId, body) => {
    return api.patch(`people/${userId}`, body)
}

export const logout = () => {
    return api.post(`people/logout`)
}

export const getUser = (userId) => {
    return api.post(`people/${userId}`)
}