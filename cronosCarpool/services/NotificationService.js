import { Permissions, Notifications } from 'expo';
import { Platform, AsyncStorage } from 'react-native'
import api from '../api';
import { store } from '../store';
import { addNotifToken } from '../actions/user-actions';
import { NavigationActions, StackActions } from 'react-navigation';
import {logout } from '../services/UserService';

export const getNotifications = userId => {
    return api.get(`people/${userId}/notifications`)
}

export const removeNotification = (userId, notifId) => {
    return api.delete(`people/${userId}/notifications/${notifId}`)
}

export const registerForPushNotificationsAsync = async (person) => {
    const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;

    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== 'granted') {
        // Android remote notification permissions are granted during the app
        // install, so this will only ask on iOS
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
    }

    // Stop here if the user did not grant permissions
    if (finalStatus !== 'granted') {
        return;
    }

    // Get the token that uniquely identifies this device
    let token = await Notifications.getExpoPushTokenAsync();

    if (Platform.OS === 'android') {
        Expo.Notifications.createChannelAndroidAsync('passenger-request', {
            name: 'Passenger Requests',
            "priority": 'high',
            "vibrate": [0, 250],
            sound: true,
        });
    }
    // POST the token to your backend server from where you can retrieve it to send push notifications.

    let val;


    
    api.patch('people/' + person.id + '?access_token=' + person["access_token"].id, { notificationToken: token })
        .then((response) => {
            store.dispatch(addNotifToken(response.data.notificationToken));
        }).catch(async error => {
            try {
                const value = await AsyncStorage.getItem('access_token');
                if (value !== null) {

                    // We have data!!
                    logout()
                        .then( async response => {

                                await AsyncStorage.removeItem('access_token');
                        })
                        .catch(async error => {
                            console.log(JSON.stringify(error));
                                await AsyncStorage.removeItem('access_token');

                        })
                }
            } catch (error) {
                console.log("KEY STORAGE: ", JSON.stringify(error))
                // Error retrieving dat
                try {
                    await AsyncStorage.removeItem('access_token');
                } catch (error) {
                    // Error retrieving data
                }
            }
        })

}
