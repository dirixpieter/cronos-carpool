import api from "../api";

export const getCars = (userId) => {

    return api.get(`people/${userId}/cars`)
}


export const removeCar = (userId, carId) => {

    return api.delete(`people/${userId}/cars/${carId}`)
}

export const createCar = (userId, body) => {
    return api.post(`people/${userId}/cars`, body)
}