import api from "../api";
import { rideListQuery } from './QueryService';


export const getRidesAsDriver = (userId, upcoming) => {
    const query = rideListQuery(upcoming);

    return api.get(`people/${userId}/asDriver?filter=${query}`)
        .then(response => { return response })
        .catch(error => { return error });
}

export const getRidesAsPassenger = (userId, upcoming) => {
    const query = rideListQuery(upcoming);

    return api.get(`people/${userId}/rides?filter=${query}`)
        .then(response => { return response })
        .catch(error => { return error });
}

export const getRide = (rideId) => {
    return api.get(`rides/${rideId}`)
        .then(response => { 
            return response 
        })
        .catch(error => { return error });
}

export const addRide = (body) => {

    return api.post(`rides`, body)
        .then(response => { return response })
        .catch(error => { return error });
}


export const findRides = (query) => {

    return api.post(`rides/findRides`, query)
        .then(response => { return response })
        .catch(error => { return error });
}

export const cancelRide = rideId => {
    return api.delete(`rides/${rideId}`)
        .then(response => { return response })
}