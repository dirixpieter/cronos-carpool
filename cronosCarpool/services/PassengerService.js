import api from "../api";
import { passengerListQuery } from './QueryService';


export const getPassengers = (rideId) => {
    const query = passengerListQuery(rideId);

    return api.get(`passengers?filter=${query}`)
}

export const acceptPassenger = (body) => {

    return api.post(`passengers/accept`, body)
}

export const denyPassenger = (body) => {

    return api.delete(`passengers/deny`, body)
        .catch(error => { return error });
}

export const requestPassenger = body => {
    return api.post(`passengers/notif`, body)
}