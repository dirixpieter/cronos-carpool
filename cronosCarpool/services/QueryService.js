import moment from 'moment';

export function rideListQuery(upcoming) {
    let query;
    if (upcoming == "upcoming") {
        query = {
            include: "passengers",
            where: {
                startTime: {
                    gte: moment()
                }
            }
        }   
    } else if (upcoming == "past") {
        query = {
            include: "passengers",
            where: {
                startTime: {
                    lte: moment()
                }
            }
        }
    }
    return encodeURI(JSON.stringify(query));
}

export const passengerListQuery = rideId => {
    let query = { "where": { "rideId": rideId }, "include": "person" }
    return encodeURIComponent(JSON.stringify(query))
}