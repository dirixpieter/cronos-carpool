import React from 'react';
import { Text, View, StyleSheet, ActivityIndicator } from 'react-native';

const Spinner = (props) => (
    <View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator size="small" color="#4286f4" />
    </View>
);

export default Spinner;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
    }
})