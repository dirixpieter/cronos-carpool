import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import moment from 'moment'
import TimeAndLocation from '../components/TimeAndLocations';

class RideListItem extends Component {


    render() {
        const { ride } = this.props



        return (
            <View style={{ marginBottom: 24 }}>
                <Text style={{ marginBottom: 16 }}>{`${moment(ride.startTime).tz('Europe/Brussels').format("dddd")}, ${moment(ride.startTime).tz('Europe/Brussels').format("LL")}`} </Text>

                <TouchableOpacity style={{ padding: 16, borderRadius: 4, borderWidth: 1, borderColor: "black" }}
                    onPress={() => this.props.handleOnPress(ride)}
                    onLongPress={() => this.props.handleLongpress(ride)}
                >

                    <TimeAndLocation ride={ride} />
                    <Text>{`Free seats: ${ride.freeSeats.toString()}`}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

export default RideListItem;
