import React from 'react';
import { Image, Text } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';


const GooglePlacesInput = (props) => {
  return (
    <GooglePlacesAutocomplete
      placeholder={props.placeholder}
      minLength={3} // minimum length of text to search
      autoFocus={false}
      returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
      listViewDisplayed='auto'    // true/false/undefined
      fetchDetails={true}
      renderDescription={row => row.description} // custom description render
      onPress={(data, details) => { // 'details' is provided when fetchDetails = true
        const { geometry } = details;
        d = {
          ...data,
          geometry: geometry,
          isStart: props.isStart
        }
        props.handleOnPress(d);
      }}

      getDefaultValue={() => ''}

      query={{
        // available options: https://developers.google.com/places/web-service/autocomplete
        key: 'AIzaSyAibRKIa1WNaDhORYI0DltaKtQmRDtFOS8',
        language: 'en', // language of the results
        types: ['geocode', 'establishment'],
        components:"country:be|country:nl" // default: 'geocode'
      }}

      styles={{
        textInputContainer: {
          width: '100%',
          backgroundColor: 'white',
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,

          elevation: 5
        },
        description: {
          fontWeight: 'bold'
        },
        predefinedPlacesDescription: {
          color: '#1faadb'
        }
      }}


      nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch

      GooglePlacesDetailsQuery={{
        // available options for GooglePlacesDetails API : https://developers.google.com/places/web-service/details
        fields: 'geometry',
      }}
      GooglePlacesSearchQuery={{
        // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
        rankby: 'distance'
      }}

      debounce={350} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.

    />
  );
}

export default GooglePlacesInput;