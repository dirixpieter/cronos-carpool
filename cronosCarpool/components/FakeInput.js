import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import { withNavigation } from 'react-navigation';

const FakeInput = (props) => (


    <Button
        title={props.text}
        buttonStyle={[styles.button, {
            marginBottom: 24
        }]}
        titleStyle={[(props.text == 'start' || props.text == 'destination')? styles.textEmpty :  styles.textFilled, {alignItems: "flex-start", textAlign: "left"}]}
        onPress={() => props.navigation.navigate('LocationInput', { isStart: props.isStart, title: props.pageTitle })}
    />

);

const styles = StyleSheet.create({
    button: {
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderColor: '#212121',
        justifyContent: 'flex-start'
    },
    textFilled: {
        color: '#212121',
        fontWeight: 'normal'
    },
    textEmpty: {
        color: '#CCCCCC',
        fontWeight: 'normal'
    }

})

export default withNavigation(FakeInput);
