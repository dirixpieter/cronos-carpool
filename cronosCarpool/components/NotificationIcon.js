import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { connect } from 'react-redux';

class NotificationIcon extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    check = () => {
        let res;
            if(this.props.new) {
                res = '#FA3E3E'
            } else {
                res = this.props.tintColor
            }

        return res;
    }

    render() {
        return (
            <View>
                <Ionicons name='md-notifications' size={25} color={this.check()} />
            </View>
        );
    }
}


function mapStateToProps(state) {
    return {
        new: state.notifications.newNotif

    };
}



export default connect(mapStateToProps, null)(NotificationIcon);
