import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Input, Divider } from 'react-native-elements';
import { TextInput } from 'react-native-gesture-handler';


class EditProfileComp extends Component {
    constructor(props) {
        super(props);
    }

    handleTextChange = text => {
        this.props.handleTextChange({
            ...this.props.person,
            [this.props.valKey]: text
        })
    }
    render() {
        return (
            <View>
                <Divider />

                <View style={styles.container}>


                    <Text style={styles.text}>{this.props.title}</Text>
                    <TextInput
                        editable={this.props.editable}
                        style={styles.textInput}
                        value={(this.props.person[this.props.valKey] != undefined) ? this.props.person[this.props.valKey] : ""}
                        onChangeText={(text) => this.handleTextChange(text)}
                    />

                </View>
                <Divider />
            </View>
        );
    }
}
const offset = 16
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        padding: offset
    },
    text: {
        alignSelf: 'center',
        marginRight: offset / 2
    },
    textInput: {
        alignSelf: 'stretch',
        fontSize: 18,
        flex: 1
    }
})


export default EditProfileComp;
