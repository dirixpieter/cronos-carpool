import React, { Component } from 'react';
import { View } from 'react-native';
import { Button, Input, Text } from 'react-native-elements';
import MapViewDirections from 'react-native-maps-directions';
import { getRegionForCoordinates } from '../classes/getRegionForCoordinates';
import MapView from 'react-native-maps';
import moment from 'moment-timezone';
import UserInfo from './UserInfo';
import axios from 'axios';
import { getRideInfo } from '../classes/GetRouteInformation';
import TimeAndLocations from './TimeAndLocations';



const RideInfo = (props) => {
    const ride = props.ride
    const userId = props.user.id;
    const mapOptions = getRegionForCoordinates([
        ride.start.geometry.location, ride.destination.geometry.location
    ])
    
    return (

        <View style={{ marginBottom: 16, marginTop: 24 }}>
            <View style={{ marginBottom: 24, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-end' }}>
                <UserInfo
                    userId={ride.driverId}
                    driverIsUser={props.driverIsUser}
                />
                <Text style={{ textAlign: 'right' }}>{moment(ride.startTime).tz('Europe/Brussels').format('YYYY-MM-DD')}</Text>
            </View>

            <TimeAndLocations ride={ride} />
            <Text style={{ fontSize: 18, fontWeight: 'bold', marginBottom: 16 }}>Details</Text>
            <View style={{ flexDirection: 'row',}}>
                <View style={{ flexDirection: 'column', flex: 1 }}>
                    <Text >- Free Seats: {ride.freeSeats.toString()} </Text>
                    <Text>- Car colour: {ride.carInfo.colour}</Text>
                </View>

                <View style={{ flexDirection: 'column', flex: 1, marginRight: 24, marginLeft: 24 }}>
                    <Text>- {ride.carInfo.model}</Text>

                    <Text>- {ride.carInfo.licensePlate}</Text>
                </View>


            </View>
            
            {(!(ride.extra == undefined)) ? <View style={{ marginTop: 16 }}>
                <Text style={{ fontSize: 18, fontWeight: 'bold', marginBottom: 16 }}>Extra</Text>
                <Text style={{ marginBottom: 16 }}>{ride.extra}</Text>

            </View> : null}

        </View>


    );

}


export default RideInfo
