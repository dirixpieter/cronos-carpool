import React from 'react';
import { Text, View } from 'react-native';
import { Button } from 'react-native-elements';
import BottomButtons from '../stylesheets/BottomButtons';

const ResultBottomButtons = (props) => (

    <View style={[BottomButtons.buttonContainer]}>
        <Button
            buttonStyle={[BottomButtons.topButton, {
                backgroundColor: "#212121",
                marginBottom:0
            }]}
            title="Request A Seat"
            onPress={() => props.handleRequestPress()}
        />
    </View>
);

export default ResultBottomButtons;
