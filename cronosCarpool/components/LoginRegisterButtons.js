import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import { withNavigation } from 'react-navigation';
import BottomButtons from '../stylesheets/BottomButtons';


class LoginRegisterButtons extends Component {
    render() {
        return (
            <View style={[BottomButtons.buttonContainer]}>


                <Button
                    buttonStyle={[BottomButtons.topButton, BottomButtons.button, {borderRadius: 4}]}
                    titleStyle={[BottomButtons.topTitle]}
                    title="Login"
                    onPress={() => {
                        this.props.navigation.navigate("Login");
                    }}
                />

                <Button
                    buttonStyle={[BottomButtons.button, BottomButtons.bottomButton, { borderColor: '#212121', borderWidth: 1, borderRadius: 4}]}
                    titleStyle={[BottomButtons.bottomTitle]}
                    title="Register"
                    onPress={() => {
                        this.props.navigation.navigate("Register");
                    }}
                />
            </View>
        )
    }


}


export default withNavigation(LoginRegisterButtons);
