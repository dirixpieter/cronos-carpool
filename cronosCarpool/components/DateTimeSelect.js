import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Picker } from 'native-base'
import DatePicker from 'react-native-datepicker';
import { onRideInputChange, onNewRoundTrip } from '../actions/ride-actions';
import { connect } from 'react-redux';
import moment from 'moment';

class DateTimeSelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            departArrival: true,
            date: moment()
        };
    }

    handleDateChange = (date) => {
        this.props.handleDateChange(date, this.props.isRoundTrip, this.state.departArrival);

    }


    render() {
        return (
            <View>
                <View
                    style={[styles.border]}
                >
                    <Picker
                        mode='dropdown'
                        selectedValue={this.state.departArrival}
                        onValueChange={(itemValue, itemIndex) => {
                            this.setState({ departArrival: itemValue })
                        }}

                    >
                        <Picker.Item label="Depart at" value={true} />
                        <Picker.Item label="Arrive by" value={false} />

                    </Picker>
                </View>
                <DatePicker
                    placeholder="Time" confirmBtnText="Confirm" cancelBtnText="Cancel" mode="datetime"
                    style={styles.datePicker}
                    date={this.props.date}
                    customStyles={{
                        dateInput: styles.dateInput,
                        dateText: styles.dateText,
                    }}
                    onDateChange={(date) => {

                        this.handleDateChange(date);
                    }
                    }
                />
            </View>
        );
    }
}
const styles = StyleSheet.create({
    border: {
        borderWidth: 1, borderColor: '#DDDDDD', marginBottom: 24
    },
    datePicker: {
        width: '100%',
        marginBottom: 24
    },
    dateInput: {
        borderWidth: 0,
        borderBottomWidth: 1,
        alignItems: 'flex-start'
    },
    dateText: {
        marginLeft: 8
    }
})
function mapStateToProps(state) {
    return {
        newRide: state.ride.newRide,
        newRoundTrip: state.ride.newRoundTrip,
        user: state.user.user,
        location: state.location,
        driverRides: state.ride.driverRides

    };
}

const mapDispatchToProps = dispatch => {
    return {
        onRideInputChange: ride => {
            dispatch(onRideInputChange(ride));
        },
        onNewRoundTrip: ride => {
            dispatch(onNewRoundTrip(ride));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DateTimeSelect);
