import React, { Component } from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { Text } from 'react-native-elements'
import moment from 'moment'
import TimeAndLocation from '../components/TimeAndLocations';

class NotificationListItem extends Component {


    render() {
        const { notif } = this.props



        return (
            <View style={styles.container}>
                <Text style={{ marginBottom: 16 }}>{moment(notif.sendDate).format('ddd, MMM Do YYYY')} at {moment(notif.sendDate).format('HH:mm')}</Text>

                <TouchableOpacity style={{ padding: 16, borderRadius: 4, borderWidth: 1, borderColor: "black" }} onPress={() => this.props.handleOnPress(notif)} onLongPress={() => this.props.handleOnLongPress(notif)}>

                    <Text style={{
                        fontWeight: "bold",
                        fontSize: 18
                    }}>{notif.title}</Text>
                    <Text>{notif.body}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: { 
        marginBottom: 24 
    }
})

export default NotificationListItem;
