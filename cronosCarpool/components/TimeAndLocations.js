import React, { Component } from 'react';
import { View, Text } from 'react-native';
import moment from 'moment';
import { Svg } from 'expo';
class TimeAndLocation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            y1: 0,
            y2: 0,
            tempHeight: 55
        }
    }

    render() {
        const { ride } = this.props

        let startTime;
        let arriveTime;

        if (ride.departAt) {
            startTime = moment(ride.startTime).tz('Europe/Brussels').format('HH:mm')
            arriveTime = moment(ride.startTime).add(ride.rideTime, 'seconds').tz('Europe/Brussels').format('HH:mm');
        } else {
            arriveTime = moment(ride.startTime, ["h:mm A"]).tz('Europe/Brussels').format('HH:mm')
            startTime = moment(ride.startTime, ["h:mm A"]).subtract(ride.rideTime, 'seconds').tz('Europe/Brussels').format('HH:mm');
        }
        const cx = 8;
        const cy1 = 8;

        const cr = 6;

        const cy2 = this.state.y2 - this.state.y1 - cr - 2;

        const lx = cx;
        const ly1 = cy1 + cr + 2;
        const ly2 = cy2 - cr - 2
        return (
            <View style={{ marginBottom: 16, flexDirection: "row" }}>
                <View style={{ marginRight: 8, flexDirection: 'column', justifyContent:'space-between' }} >
                    <Text

                        numberOfLines={1}
                        style={{ marginBottom: 16 }}>



                        {startTime}
                    </Text>

                    <Text
                        
                        numberOfLines={1}
                        style={{ marginBottom: 0, alignSelf: 'flex-end' }}
                    >

                        {arriveTime}
                    </Text>
                </View>
                <Svg width={16} height={this.state.y2 - this.state.y1}>
                    <Svg.Circle
                        cx={cx}
                        cy={cy1}
                        r={cr}
                        fill="#212121"
                    />
                    <Svg.Line
                        x1={lx}
                        y1={ly1}
                        x2={lx}
                        y2={ly2}
                        stroke="#212121"
                        strokeWidth={2}
                    />
                    <Svg.Circle
                        cx={cx}
                        cy={cy2}
                        r={cr}
                        strokeWidth={1}
                        stroke="#212121"
                        fill="#FFFFFF"
                    />
                </Svg>
                <View style={{ marginLeft: 8, flexDirection: 'column', flexShrink: 1 }}>
                    <Text
                        style={{ marginBottom: 16, flexWrap: 'wrap',  }}
                        onLayout={event => this.setState({ y1: event.nativeEvent.layout.y })}>
                        {ride.start.description}
                    </Text>
                    <Text
                        style={{ flexWrap: 'wrap' }}
                        onLayout={event => this.setState({ y2: event.nativeEvent.layout.y + event.nativeEvent.layout.height })}
                    >
                        {ride.destination.description}
                    </Text>
                </View>

            </View>
        );
    }
}

export default TimeAndLocation;
