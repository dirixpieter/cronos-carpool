import React, { Component } from 'react';
import api from '../api';
import { connect } from 'react-redux'
import { View } from 'react-native';
import { Text } from 'react-native-elements';
import { getUser } from '../services/UserService';

class UserInfo extends Component {
    state = {
        currentUser: false,
        userInfo: {
            name: "",
            surname: ""
        }
    }



    componentDidMount = () => {
        if (!this.props.driverIsUser) {
            getUser(this.props.userId)
                .then((response) => {
                    this.setState({
                        userInfo: response.data
                    })
                })
                .catch((error) => {
                    console.log(JSON.stringify(error))
                })
        } else {
            this.setState({
                currentUser: true,
                userInfo: this.props.user
            })
        }
    }

    render = () => {
        const user = this.state.userInfo;
        return (
            <Text style={{ fontSize: 18 }}>Driver: {user.name} {user.surname}{this.props.driverIsUser ? " (You)" : ""}</Text>
        )

    }
}
function mapStateToProps(state) {
    return {
        user: state.user.user
    };
}

const mapDispatchToProps = dispatch => {
    return {
        setCurrentLocation: location => {
            dispatch(setCurrentLocation(location));
        },
        setMarkers: markers => {
            dispatch(setMarkers(markers));
        }
    };
};

export default connect(mapStateToProps, null)(UserInfo)