import * as types from './action-types';

export function onLoginInputChange(user) {
    return {
        type: types.ON_LOGIN_INPUT_CHANGE,
        payload: user
    }
}

export function onRegisterInputChange(user) {
    return {
        type: types.ON_REGISTER_INPUT_CHANGE,
        payload: user
    }
}

export function onLoginSuccess(data) {
    return {
        type: types.LOGIN_SUCCES,
        payload: data
    }
}

export function currentUser(user) {
    return {
        type: types.CURRENT_USER,
        payload: user
    }
}

export function addNotifToken(token) {
    return {
        type: types.ADD_NOTIFTOKEN,
        payload: token
    }
}

export function setNotifications(notifications) {
    return {
        type: types.SET_NOTIFICATIONS,
        payload: notifications
    }
}

export function newNotif(notifications) {
    return {
        type: types.NEW_NOTIF,
        payload: notifications
    }
}

export function setCars(cars) {
    return {
        type: types.SET_CARS,
        payload: cars
    }
}

export function onCarInputChange(car) {
    return {
        type: types.ON_CAR_INPUT_CHANGE,
        payload: car
    }
}