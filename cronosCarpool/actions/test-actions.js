import * as types from './action-types';


export function setTest(text) {
    return {
        type: types.TEST,
        payload: text
    }
}
