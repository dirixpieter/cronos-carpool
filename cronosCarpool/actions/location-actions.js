import * as types from './action-types';

export function setCurrentLocation(loc) {
    return {
        type: types.SET_CURRENT_LOCATION,
        payload: loc
    }

}

export function setMarkers(markers) {
    return {
        type: types.SET_MARKERS,
        payload: markers
    }

}