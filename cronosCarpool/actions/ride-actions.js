import * as types from './action-types';

export function onRideInputChange(ride) {
    return {
        type: types.ON_RIDE_INPUT_CHANGE,
        payload: ride
    }

}
export function onNewRoundTrip(ride) {
    return {
        type: types.NEW_ROUND_TRIP,
        payload: ride
    }

}

export function setRidesAsDriver(rides) {
    return {
        type: types.SET_RIDES_AS_DRIVER,
        payload: rides
    }
}

export function setRidesAsPassenger(rides) {
    return {
        type: types.SET_RIDES_AS_PASSENGER,
        payload: rides
    }
}

export function nrOfPassengers(nr) {
    return {
        type: types.NR_OF_PASSENGERS,
        payload: nr
    }
}

export function findRideQuery(ride) {
    return {
        type: types.FIND_RIDE_QUERY,
        payload: ride
    }

}

export function setResults(rides) {
    return {
        type: types.SET_RESULTS,
        payload: rides
    }

}

export const setPassengers = (passengers) => ({
  type: types.SET_PASSENGERS,
  payload: passengers
})
