import axios from 'axios';

export default axios.create({
  baseURL: 'https://interns-carpool.appspot.com/api/'
});